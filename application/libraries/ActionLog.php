<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * SiteLogging Class
 */
class ActionLog {
	/**
	 * @var self
	 */
	protected static $instance;

	/**
	 * @var CI_Controller
	 */
	protected $ci;

	/**
	 *
	 * @var Actionlog_model
	 */
	protected $model;

	protected $excludeList = array();

	/**
	 *
	 * @return self
	 */
	public static function getInstance() {
		if (!isset(self::$instance)) {
			self::$instance = 1; // prevent reassign in constructor
			self::$instance = new static();
		}

		return self::$instance;
	}

	/**
	 * Constructor
	 */
	public function __construct() {
		// return if isntance already created
		// as always return same initialised object
		if(self::$instance instanceof self) {
			return;
		}

		// CI instance
		$this->ci =& get_instance();

		// load model
		$this->ci->load->model('actionlog_model', '', true);

		$this->model = $this->ci->actionlog_model;

		// assign initialised object if not yet set
		if (!isset(self::$instance)) {
			self::$instance = $this;
		}
	}

	/**
	 * Add item to not to log list
	 * @return self
	 */
	public function addExcludeEntry($entry) {
		$this->excludeList[] = $entry;
		return $this;
	}

	/**
	 * check is entry in not to log list
	 * @return bool
	 */
	public function isExcludeEntry($entry) {
		return !(empty($this->excludeList) || !in_array($entry, $this->excludeList));
	}


	public function logLogsExport($userId=null) {
		return $this->addLog('logs.export', 0, $userId);
	}


	public function logUserLogin($userId) {
		return $this->addLog('user.login', $userId, $userId);
	}

	public function logUserLogout($userId) {
		return $this->addLog('user.logout', $userId, $userId);
	}

	public function logUserDelete($targetId, $data, $userId=null) {
		return $this->addLog('user.delete', $targetId, $userId, $data);
	}

	public function logUserAdd($targetId, $data, $userId=null) {
		return $this->addLog('user.add', $targetId, $userId, $data);
	}

	public function logUserEdit($targetId, $data, $userId=null, $dataOld=null) {
		return $this->addLog('user.edit', $targetId, $userId, $data, $dataOld);
	}


	public function logGroupExport($targetId, $data, $userId=null) {
		return $this->addLog('groups.export', $targetId, $userId, $data);
	}

	public function logGroupDelete($targetId, $data, $userId=null) {
		return $this->addLog('group.delete', $targetId, $userId, $data);
	}

	public function logGroupAdd($targetId, $data, $userId=null) {
		return $this->addLog('group.add', $targetId, $userId, $data);
	}

	public function logGroupEdit($targetId, $data, $userId=null, $dataOld=null) {
		return $this->addLog('group.edit', $targetId, $userId, $data, $dataOld);
	}

	public function logGroupResetTap($targetId, $data, $userId=null) {
		return $this->addLog('group.reset.tap', $targetId, $userId, $data);
	}

	public function logGroupAssignUser($targetId, $data, $userId=null) {
		return $this->addLog('group.assign.user', $targetId, $userId, $data);
	}

	public function logGroupDissociateUser($targetId, $data, $userId=null) {
		return $this->addLog('group.dissociate.user', $targetId, $userId, $data);
	}


	public function logProtegegxRefreshUsers($userId=null) {
		return $this->addLog('protegegx.refreshusers', 0, $userId);
	}

	public function logProtegegxRefreshUsersEnd($data, $userId=null) {
		return $this->addLog('protegegx.refreshusers.end', 0, $userId, $data);
	}

	public function logProtegegxApiGetRecord($data, $userId=null) {
		return $this->logProtegegxApiAction('GetRecord', $data, $userId);
	}

	public function logProtegegxApiListRecords($data, $userId=null) {
		return $this->logProtegegxApiAction('ListRecords', $data, $userId);
	}

	public function logProtegegxApiUpdateRecord($data, $userId=null) {
		return $this->logProtegegxApiAction('UpdateRecord', $data, $userId);
	}

	public function logProtegegxApiFindRecords($data, $userId=null) {
		return $this->logProtegegxApiAction('FindRecords', $data, $userId);
	}

	public function logProtegegxApiGetLatestEvents($data, $userId=null) {
		return $this->logProtegegxApiAction('GetLatestEvents', $data, $userId);
	}

	protected function logProtegegxApiAction($action, $data, $userId=null) {
		if(isset($data['L'])) {
			$data['L'] = '<Hide For Security Concerns>';
		}

		if(isset($data['LD'])) {
			$data['LD'] = '<Hide For Security Concerns>';
		}

		return $this->addLog('protegegx.api.' . $action, 0, $userId, $data);
	}


	protected function addLog($action, $target, $userId=null, $data = null, $dataOld=null) {
		if($this->isExcludeEntry($action)) {
			return $this;
		}

		$this->model->addEntry($action, $target, $userId, $data, $dataOld);

		return $this;
	}

}
