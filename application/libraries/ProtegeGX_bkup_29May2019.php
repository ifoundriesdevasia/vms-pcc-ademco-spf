<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * ProtegeGX Class
 */
class ProtegeGX {

	/**
	 * @var ProtegaGX[]  An array of ProtegaGX instances.
	 */
	protected static $instances = array();

	/**
	 * @var array Default config
	 */
	protected static $config;

	/**
	 * @var CI_Controller
	 */
	protected $ci;

	/**
	 * @var array
	 */
	protected $cacheTimes = array(
		'get'	=> 900,
		'list'	=> 900,
		'find'	=> 900,

		'get_custom_field'	=> 86400,
		'list_user'	=> 900,
		'get_user'	=> 300,
	); // 15 minnutes

	/**
	 * @var int
	 */
	protected $cacheTime = 900; // 15 minnutes

	/**
	 * @var SoapClient
	 */
	protected $client;

	/**
	 * @var array Login credentials to be used in communicate to soap server
	 */
	protected $login;

	public static function getInstance($config = array(), $name = 'default') {
		if (!isset(self::$instances[$name])) {
			self::$instances[$name] = new static($config);
//			self::$instances[$name] = new ProtegeGX($config);
		}

		return self::$instances[$name];
	}

	/**
	 * Constructor
	 *
	 * @param   array  $config  An array of configuration options (name, state, dbo, table_path, ignore_request).
	 *
	 * @throws  Exception
	 */
	public function __construct($config = array()) {
		// CI instance
		$this->ci =& get_instance();

		// load cache
		$this->ci->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));

		// load config
		$this->loadConfig();

		// set variables
		if(array_key_exists('gx_url', $config)) {
			self::$config['gx_url'] = $config['gx_url'];
		}

		if(array_key_exists('gx_username', $config)) {
			self::$config['gx_username'] = $config['gx_username'];
		}

		if(array_key_exists('gx_password', $config)) {
			self::$config['gx_password'] = $config['gx_password'];
		}

		if(array_key_exists('win_username', $config)) {
			self::$config['win_username'] = $config['win_username'];
		}

		if(array_key_exists('win_password', $config)) {
			self::$config['win_password'] = $config['win_password'];
		}

		$login = new stdClass();
		$login->LoginType = 0;
		$login->UserName = self::$config['gx_username'];
		$login->Password = self::$config['gx_password'];

		$this->login = $login;
	}

	protected function loadConfig() {
		if(!isset(self::$config)) {
			$ci = $this->ci;
			$ci->config->load('ProtegeGX');

			self::$config['gx_url'] = $ci->config->item('gx_url')?
				$ci->config->item('gx_url') : '';

			self::$config['gx_username'] = $ci->config->item('gx_username')?
				$ci->config->item('gx_username') : '';

			self::$config['gx_password'] = $ci->config->item('gx_password')?
				$ci->config->item('gx_password') : '';

			self::$config['win_username'] = $ci->config->item('win_username')?
				$ci->config->item('win_username') : '';

			self::$config['win_password'] = $ci->config->item('win_password')?
				$ci->config->item('win_password') : '';
		}
	}

	/**
	 * Create soap client
	 * @param bool $reset	Re-initialise soap instance
	 * @throws Exception
	 */
	protected function initSoapClient($reset=false) {
		if($reset || !isset($this->client)) {
			try {
				$this->client = new SoapClient(self::$config['gx_url'], array('login' => self::$config['win_username'], 'password' => self::$config['win_password']));
			}
			catch (Exception $ex) {
				if($ex instanceof SoapFault) {
					show_error("SOAP Fault: (faultcode: {$ex->faultcode}, faultstring: {$ex->faultstring})", 500);
					throw new Exception("SOAP Fault: (faultcode: {$ex->faultcode}, faultstring: {$ex->faultstring})", 500);
				}
				else {
					show_error('SOAP Exception: (' . $ex->getCode() . ') ' . $ex->getMessage(), 500);
					throw new Exception('SOAP Exception: (' . $ex->getCode() . ') ' . $ex->getMessage(), 500);
				}
			}
		}
	}

	/**
	 * Make soap call.
	 * @param string $method	Soap function.
	 * @param array $data	Data to be passed in soap function.
	 * @return mixed
	 * @throws Exception
	 */
	protected function doSoapCall($method, $data) {
		if(!isset($this->client)) {
			$this->initSoapClient();
		}

		// try make call
		try {
			$re = $this->client->$method($data);
		}
		catch (Exception $ex) {
			if($ex instanceof SoapFault) {
				show_error("SOAP Fault: (faultcode: {$ex->faultcode}, faultstring: {$ex->faultstring})", 500);
				throw new Exception("SOAP Fault: (faultcode: {$ex->faultcode}, faultstring: {$ex->faultstring})", 500);
			}
			else {
				show_error('SOAP Exception: (' . $ex->getCode() . ') ' . $ex->getMessage(), 500);
				throw new Exception('SOAP Exception: (' . $ex->getCode() . ') ' . $ex->getMessage(), 500);
			}
		}

		// check return error
		if(!empty($re->nErrorCode)) {
			show_error("Error to call SOAP function, {$method}(). Error Code: " . $re->nErrorCode, $re->nErrorCode);
			throw new Exception("Error to call SOAP function, {$method}(). Error Code: " . $re->nErrorCode, $re->nErrorCode);
		}

		return $re;
	}

	protected function getCacheName() {
		$args = array_merge(func_get_args(), array(__FUNCTION__));
		return strtolower(__CLASS__) .'-'. md5(implode('.', $args));
	}


	public function listRecordsCustomFields($start=0, $numRows=10) {
		return $this->listRecords(245, $start, $numRows);
	}

	public function listRecordsUsers($start=0, $numRows=10) {
		return $this->listRecords(100, $start, $numRows);
	}

	public function getRecordCfDesignation() {
		$r = $this->getRecordCustomField(15);

		if(false === $r) {
			return array();
		}

		return $r->_formdata;
	}

	public function getRecordCfDepartment() {
		$r = $this->getRecordCustomField(16);

		if(false === $r) {
			return array();
		}

		return $r->_formdata;
	}

	public function getRecordCfDivision() {
		$r = $this->getRecordCustomField(17);

		if(false === $r) {
			return array();
		}

		return $r->_formdata;
	}

	public function getRecordCfTeam() {
		$r = $this->getRecordCustomField(18);

		if(false === $r) {
			return array();
		}

		return $r->_formdata;
	}

	public function getRecordCfCompany() {
//		$r = $this->getRecordCustomField(16);
//
//		if(false === $r) {
//			return array();
//		}
//
//		return $r->_formdata;

		return array();
	}

	public function getRecordCfUserType() {
		return array(
			0	=> 'Escort',
			1	=> 'Visitor',
		);
	}

	public function getRecordCustomField($recordID) {
		$sig = $this->getCacheName(__METHOD__, $recordID);

		if(false === ($cache = $this->ci->cache->get($sig))) {
			$r = $this->getRecord(245, $recordID);

			if(false === $r) {
				return false;
			}

			// save
			if(!empty(strXML) || !empty($r->_xml)) {
				$r->strXML = $r->_xml->asXML();
				unset($r->_xml);
			}

			$cache = $r;

			$this->ci->cache->save($sig, $cache, $this->cacheTimes['get_custom_field']);
		}

		// xml
		if(!empty($cache->strXML)) {
			$cache->_xml = simplexml_load_string($cache->strXML);
			if(false === $cache->_xml) {
				return false; //invalid xml
			}
		}

		// set common data
		$formdata  = array();
		if($cache->_xml instanceof SimpleXMLElement && !empty($cache->_xml->CustomFieldListGroupData->CustomFieldListGroupDataData)) {
			// $cache->_xml->FieldType; // 7: dropdown
			$d = $cache->_xml->CustomFieldListGroupData->CustomFieldListGroupDataData;

			foreach($d as $v) {
				$formdata[(string) $v->ActualValue] = (string) $v->DisplayName;
			}
		}

		$cache->_formdata = $formdata;

		return $cache;
	}

	public function getRecordUser($recordID) {
		$r = $this->getRecord(100, $recordID);

		if(false === $r) {
			return false;
		}

		$data = array();

		// set common data
		$cards = array();
		if(!empty($r->_xml->vUserCardNumberGroupData->UserCardNumberGroupDataData)) {
			$cardDetails = $r->_xml->vUserCardNumberGroupData->UserCardNumberGroupDataData;

			foreach($cardDetails as $cardDetail) {
				$cards[] = array(
					'CardNumber'	=> (string) $cardDetail->CardNumber,
					'FamilyNumber'	=> (string) $cardDetail->FamilyNumber,
					'Disabled'	=> ('false' === (string) $cardDetail->CardDisabled)? 0 : 1,
				);
			}

			// get the first row
//			list($cardNum, $familyNum, $cardDisabled) = $cards[0]; // list () does not work with assoc arrray
			list($cardNum, $familyNum, $cardDisabled) = array_values($cards[0]);
		}
		else {
			$cardDisabled = 0;
			$cardNum = '';
			$familyNum = '';
		}

		// custom fields
		$assocCfIds = array(
			12	=> 'IsVisitor',
			14	=> 'Nric',
			15	=> 'Designation',
			16	=> 'Department',
			17	=> 'Division',
			18	=> 'Team',
			20	=> 'OfficeNumber',
			21	=> 'MobileNumber',
			22	=> 'Email',
			57	=> 'Company',
		);

//		$cfData = $r->_xml->vUserCustomFieldGroupData->UserCustomFieldGroupDataData; // same?
		$cfData = $r->_xml->UserCustomFieldGroupData->UserCustomFieldGroupDataData;

		foreach($cfData as $cf) {
			$fid = (int) $cf->CustomFieldID;
			if(!isset($assocCfIds[$fid])) {
				continue;
			}

			$k = $assocCfIds[$fid];

			if(12 === $fid) {
				$val = ('false' === (string) $cf->CustomFieldBooleanData)? 0 : 1;
			}
			else {
//				$count = $cf->CustomFieldTextData->count(); // PHP > 5.3
//				$count = count($cf->CustomFieldTextData->children()); // PHP < 5.3
//
//				if($count) {
//					$arr = (array) $cf->CustomFieldTextData;
//					$val = isset($arr[0])? (string) $arr[0] : '';
//				}
//				else {
//					$val = (string) $cf->CustomFieldTextData;
//				}

				// CustomFieldNumericalData
				$fldType = (string) $cf->CustomFieldType;
				if('7' === $fldType) {
					// dropdown
					// will try to get data via API
					// if no data return or no match then use CustomFieldNumericalData
					$val = (string) $cf->CustomFieldNumericalData;
					$method = "getRecordCf{$k}";
					if(method_exists($this, $method)) {
						$cfOptions = $this->$method();
						if(isset($cfOptions[$val])) {
							$val = $cfOptions[$val];
						}
					}
				}
				else {
					$val = (string) $cf->CustomFieldTextData;
				}
			}

			$data[$k] = $val;
		}

		$data['group_id'] = 0;
		$data['UserID'] = (string) $r->_xml->Index;
		$data['Name'] = (string) $r->_xml->Name;
		$data['FirstName'] = (string) $r->_xml->FirstName;
		$data['LastName'] = (string) $r->_xml->LastName;
		$data['DisableUser'] = ('false' === (string) $r->_xml->DisableUser)? 0 : 1;
		$data['CardNumber'] = $cardNum;
		$data['FamilyNumber'] = $familyNum;
		$data['CardDisabled'] = $cardDisabled;
		$data['_cards'] = $cards;

		$r->_data = $data;

		return $r;
	}

	public function findRecordsUsers($keyword, $sfield, $contain=false) {
		$sFields = array(
			'FirstName',
			'LastName',
			'Name',
			'CardNumber',
		);

		if(!in_array($sfield, $sFields)) {
			$sfield = 'Name';
		}

		return $this->findRecords(100, $keyword, $sfield, $contain);
	}

	public function findRecords($tableID, $keyword, $sfield, $contain=false) {
		if('' == trim($keyword)) {
			return false;
		}

		$data = array(
			'L'	=> $this->login,
			'nTableID'	=> $tableID,
			'nParent'	=> 1,
			'nPage'	=> 0,
			'nSortField'	=> 2,
			'bSortDirection'	=> 'true',

			'strFieldName'	=> $sfield,
			'bContain'	=> (bool) $contain? 1 : 0,
			'strSearchString'	=> $keyword,

			'nMinValue'	=> -1,
			'nMaxValue'	=> -1,

			'startDate'	=> '0001-01-01',
			'endDate'	=> '0001-01-01',
			'List'	=> array(),
		);

		// add action log
//		ActionLog::getInstance()->logProtegegxApiFindRecords($data);

		// get & save cache
		$sig = $this->getCacheName(__METHOD__, implode(',', $data));

		if(false === ($cache = $this->ci->cache->get($sig))) {
			$cache = $this->doSoapCall('FindRecords', $data);
			$this->ci->cache->save($sig, $cache, $this->cacheTimes['find']);
		}

		return $cache;
	}

	public function getRecord($tableID, $recordID) {
		$data = array(
			'LD'	=> $this->login,
			'nTableID'	=> $tableID,
			'nParentID'	=> 1,
			'nRecordID'	=> $recordID,
		);

		// add action log
//		ActionLog::getInstance()->logProtegegxApiGetRecord($data);

		$r = $this->doSoapCall('GetRecord', $data);

		if(empty($r->GetRecordResult)) {
			return false;
		}

		$r->_xml = simplexml_load_string($r->strXML);

		if(false === $r->_xml) {
			return false; //invalid xml
		}

		return $r;
	}

	public function listRecords($tableID, $start=0, $numRows=10) {
		$data = array(
			'LD'	=> $this->login,
			'nTableID'	=> $tableID,
			'nParentID'	=> 1,
			'nStart'	=> $start,
			'nRecordGroup'	=> 2147483647, // UserAccessLevelSchedule
			'nNumberOfRows'	=> $numRows,
		);

		// add action log
//		ActionLog::getInstance()->logProtegegxApiListRecords($data);

		return $this->doSoapCall('ListRecords', $data);
	}

	public function getLatestEventsUsers($limit=1) {
		return $this->getLatestEvents(3, $limit);
	}

	/**
	 *
	 * @param int $reportID	Report ID
	 * @param int $limit	Number of recent events requested
	 * @return type
	 * @throws Exception
	 */
	public function getLatestEvents($reportID, $limit=1) {
		$data = array(
			'LD'	=> $this->login,
			'strWorkStation'	=> null,
			'nSiteID'	=> 1,
			'nReportID'	=> $reportID,
			'nMostRecentCount'	=> $limit,
		);

		// 2 additional params not in used
		// nLastID		Last known retrieved event ID
		// nLastAckID	Last known retrieved event ID which is acknowledged

		// add action log
//		ActionLog::getInstance()->logProtegegxApiGetLatestEvents($data);

		return $this->doSoapCall('GetLatestEvents', $data);
	}

	public function updateRecordUser($recordID, $strXml) {
		return $this->updateRecord(100, $recordID, $strXml);
	}

	public function updateRecord($tableID, $recordID, $strXml) {
		if(is_string($strXml)) {
			$strXml = @simplexml_load_string($strXml);
		}

		if($strXml instanceof SimpleXMLElement) {
			$strXml = $strXml->asXML();
		}
		else {
			show_error('updateRecord() Error: Valid XML is required', 500);
			throw new Exception('updateRecord() Error: Valid XML is required');
		}

		$data = array(
			'LD'	=> $this->login,
			'nTableID'	=> $tableID,
			'nParentID'	=> 1,
//			'nRecordGroup'	=> 2147483647, // not required in doc
			'nRecordID'	=> $recordID,
			'strXML' => $strXml,
		);

		// add action log
//		ActionLog::getInstance()->logProtegegxApiUpdateRecord($data);

		return $this->doSoapCall('UpdateRecord', $data);
	}

	public function disableUser($recordID, $status) {
		if((bool) $status) {
			$strXml = <<<EOD
<?xml version='1.0'?>
<UsersData xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>
	<DisableUser>true</DisableUser>
</UsersData>
EOD;

		}
		else {
$strXml = <<<EOD
<?xml version='1.0'?>
<UsersData xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>
	<DisableUser>false</DisableUser>
</UsersData>
EOD;
		}

		return $this->updateRecordUser($recordID, $strXml);
	}

}
