<?php
/**
 * Main class for sugar crm api
 *
 * @version 0.5.10
 * @author Atul Dhanawat
 */
class SugarCRM
{
	public function __construct()
	{
		$CI =& get_instance();
		require_once(APPPATH.'libraries/sugarcrm/vendor/autoload.php');
		$CI->load->library( 'sugarcrm/Asakusuma/SugarWrapper/Rest' );
	}
}
?>
