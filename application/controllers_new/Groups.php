<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//date_default_timezone_set('America/New_York');

class Groups extends CI_Controller {
	public function __construct() {
		parent::__construct();
		if (!$this->session->userdata('logged_in')) {
			// Allow some methods?
			$allowed = array();
			if (!in_array($this->router->fetch_method(), $allowed)) {
				redirect('users/login');
			}
		}

		$user = $this->session->userdata('logged_in');
		$this->userID = $user['id'];
		$this->isSuper = $user['isSuper'];
		$this->output->set_template('default');

		$this->load->model('group', '', TRUE);
		$this->load->model('group_user', '', true);
		$this->load->model('Permission_Model', 'permissions', true);
	}

	public function index() {
		$data['userrole'] = $this->permissions->edit($this->isSuper);
        $this->page = 'group';
        // chkaccess($userrole,'operator','create',true)
        chkaccess($data['userrole'],$this->page,'view');

		$this->output->set_template('default');

		// groups/index/20
		$pageOffset = (int) $this->uri->segment(3);

		$data['groupInfoUrl'] = site_url('groups/groupTapInfo/');

		$params = array();

		// load data
		$pageOffset = (!empty($pageOffset) && $pageOffset > 0)? $pageOffset : 0;
		$perpage = 20;
		$data['total'] = $this->group->getListTotal($params);
		$data['results'] = $this->group->getList($params, $perpage, $pageOffset);

		// pagination
		$data['pagelinks'] = WHelper::getPaginationHtml(site_url('groups/index/'), $data['total'], $perpage, 3);

		$this->load->view('groups/index', $data);
	}

	public function cronscheduler() {
		$this->output->set_template('blank');
		$url = base_url('users/cronscheduler');
		for ($i = 1; $i > 0; $i++) {
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_exec($ch);
			curl_close($ch);
			echo '<br/>';
			sleep(3);
		}
	}

	//Regiatration by post and regaster form
	public function add() {
		$this->output->set_template('default');
//		$this->output->set_title('SPF :: Add New Group');

		$data = array();
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			$this->form_validation->set_rules('name', 'Group Name', 'trim|required');
			if ($this->form_validation->run() == FALSE) {
				// do nothing
			}
			else {
				$data = array(
					'group_name' => $this->input->post('name', TRUE),
				);

				//Save registration date in CodeIgniter
				$result = $this->group->insert_data($data);

				if ($result == TRUE) {
					$this->session->set_flashdata('success', 'Group added.');
					redirect('groups/index');
				}
				else {
					$this->session->set_flashdata('error', 'Unable to save Group.');
				}
			}
		}

		$this->load->view('groups/add', $data);
	}

	public function update() {
		$id = $this->uri->segment(3);
		if (empty($id)) {
			redirect('groups/add');
		}

		$this->output->set_template('default');
//		$this->output->set_title('SPF :: Update Group');

		$data = array();
		$data['result'] = $this->group->get_data($id);
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			$this->form_validation->set_rules('name', 'Group Name', 'trim|required');
			if ($this->form_validation->run() == FALSE) {
				// do nothing
			} else {
				$values = array(
					'group_name' => $this->input->post('name', TRUE),
				);

				// save group
				$id = $this->input->post('id', TRUE);
				$result = $this->group->update_data($values, $id);

				$data['result'] = $this->group->get_data($id, true);
				if ($result == TRUE) {
					$this->session->set_flashdata('success', 'Group updated.');
				} else {
					$this->session->set_flashdata('error', 'Unable to update group.');
				}
			}
		}

		$this->load->view('groups/update', $data);
	}

	public function delete() {
		$id = $this->uri->segment(3);
		if (empty($id)) {
			redirect('groups');
		}

		$result = $this->group->delete($id);
		if ($result == TRUE) {
			$this->session->set_flashdata('success', 'Group deleted.');
		} else {
			$this->session->set_flashdata('error', 'Unable to delete group.');
		}

		redirect('groups');
	}

	public function resetUsers() {
		$this->output->set_template('blank');
		$gid = $this->uri->segment(3);
		if($this->group_user->resetUsersTap($gid)) {
			$this->session->set_flashdata('success', 'This group has been reset.');
			$this->session->keep_flashdata('success');
		}
		else {
			$this->session->set_flashdata('error', 'This group has failed to reset.');
			$this->session->keep_flashdata('error');
		}		if(strpos($_SERVER['HTTP_REFERER'], 'groups') !== false) {			redirect('groups');			exit;		}		
		redirect('group-users/reassing/' . $gid);
	}

	/**
	 * get group(s) info
	 * currently this return json format
	 * @return type
	 */
	public function groupTapInfo() {
		$this->output->set_template('blank');

		$data = array();
		$ids = $this->input->post_get('id');
		$loadGrpUser = (bool) $this->input->post_get('loaduser');

		if(empty($ids)) {
			echo json_encode(array('items' => $data));
			exit;
		}

		if(!is_array($ids)) {
			$ids = array((int) $ids);
		}

		$ids = array_unique($ids);
		foreach($ids as $id) {
			$row = $this->group->getData($id);
			if(empty($row)) {
				continue;
			}

			$row = $row[0];

			// load group users
			if($loadGrpUser) {
				$gusers = $this->group_user->get_data($row->id);
				foreach($gusers as $k => $guser) {
					$gusers[$k] = (object) array(
						'id'	=> $guser->id,
						'groupId'	=> $guser->group_id,
						'userId'	=> $guser->user_id,
						'name'	=> $guser->name,
						'cardNumber'	=> $guser->card_number,
						'familyNumber'	=> $guser->family_number,
						'hasTap'	=> $guser->has_tap,
						'isVisitor'	=> $guser->cf_visitor,
						'cardDisabled'	=> $guser->card_disabled,
						'tapStatus'	=> $guser->has_tap? 'IN' : 'OUT',
					);
				}

				$row->users = $gusers;
			}

			$data[] = $row;
		}

		echo json_encode(array('items' => $data));
	}

	public function syncAllGroupTapInfo() {
		$rows = $this->group->getData();
		foreach($rows as $row) {
			$this->group->updateAllTapCount($row->id);
		}
		echo '1';
	}

	public function export() {
		// possible values:
		// groups/export/all/20
		// groups/export/123/20
		$gid = $this->uri->segment(3);
		$pageOffset = (int) $this->uri->segment(4);

		$postForm = $this->input->post('form');

		$data = array();
		$params = array();

		// check date range by form values
		if(!empty($postForm)) {
			$pageOffset = 0;

			// group ID
			if(!empty($postForm['group'])) {
				$gid = $postForm['group'];
			}
		}

		// check group ID
		if(empty($gid)) {
			$gid = 'all';
		}

		// real group ID
		$realGid = is_numeric($gid)? $gid : null;

		// assign it to post for set_value()
		$_POST['form']['group'] = $realGid;


		// load data
		$pageOffset = isset($pageOffset)? (int) $pageOffset : 0;
		$perpage = 20;
		$data['total'] = $this->group_user->getCsvDataTotal($realGid);

		// group users
		$data['data'] = array();
		$gusers = $this->group_user->getCsvData($realGid, $perpage, $pageOffset);
		foreach($gusers as &$guser) {
			unset($guser->xml);
			$guser->userType = $guser->cf_visitor? 'Visitor' : 'Escort';
		}
		$data['data'] = $gusers;

		// pagination
		$data['pagelinks'] = WHelper::getPaginationHtml(site_url('groups/export/' . $gid . '/'), $data['total'], $perpage, 4);


		// export link
		$data['exportLink'] = site_url('groups/doExport/' . $realGid);


		// form data
		$ddlAttr = array(
			'class'	=> 'select2 form-control',
			'data-minimum-results-for-search'	=> '10',
		);

		$formdata = array(
			'groups'	=> array(
				'' => '- All -',
			),
		);

		// form data - groups
		$groups = $this->group->getList();
		foreach($groups as $group) {
			$formdata['groups'][$group->id] = $group->group_name;
		}

		$data['formhtml'] = array(
			'group'	=> form_dropdown('form[group]', $formdata['groups'], set_value('form[group]', ''), $ddlAttr),
		);

		$this->load->view('groups/export', $data);
	}

	public function doExport() {
		set_time_limit(0); // no timeout

		$this->output->set_template('blank');
		$this->load->helper('csv');

		$id = null;
		if ($this->uri->segment(3)) {
			$id = $this->uri->segment(3);
		}

		// group
		$groupData = null;
		if($id > 0) {
			$groupData = $this->group->getData($id);
		}

		// add action log
		ActionLog::getInstance()->logGroupExport($id, $groupData? $groupData[0] : null);

		// temp directory
		$tmpPath = FCPATH . 'tmp';
		if(!is_dir($tmpPath)) {
			mkdir($tmpPath, 0755);
		}

		// clean old files with 1/30 chance
		if(WHelper::hitOrMiss(30)) {
			WHelper::cleanDirectory($tmpPath, 1);
		}

		// file name & path
		$fileName = 'ExportData-' . date('YmdHis');
		$filePath = $tmpPath . '/' . $fileName . mt_rand(1, 999999) . '.csv';


		// row header
		$header = array(
			'Group Name',
			'User Name',
			'NRIC',
			'Designation',
			'Department',
			'Division',
			'Team',
			'Company',
			'Disabled',
			'Card Number',
			'Family Number',
			'Type'
		);

		write_file($filePath, array_to_csv(array($header)), 'a');


		// total rows
		$total = $this->group_user->getCsvDataTotal($id);
		if($total) {
			$offset = -1;
			$perrun = 300;
			$maxrun = ceil($total / $perrun);

			for($i=0; $i<$maxrun; $i++) {
				$store = array();
				$offset = ($offset === -1)? 0 : $offset + $perrun;
				$csvDatas = $this->group_user->getCsvData($id, $perrun, $offset);

				foreach($csvDatas as $csv) {
					$store[] = array(
						$csv->group_name,
						$csv->name,
						$csv->cf_nric,
						$csv->cf_designation,
						$csv->cf_department,
						$csv->cf_division,
						$csv->cf_team,
						$csv->cf_company,
						$csv->user_disabled,
						$csv->card_number,
						$csv->family_number,
						$csv->cf_visitor? 'Visitor' : 'Escort',
					);
				}

				// write rows
				write_file($filePath, array_to_csv($store), 'a');
			}
		}


		header('Content-Type: application/csv');
		header('Content-Disposition: attachement; filename="' . $fileName . '.csv"');
		echo file_get_contents($filePath);
		exit;
	}

}
