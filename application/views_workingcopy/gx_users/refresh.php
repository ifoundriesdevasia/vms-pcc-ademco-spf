<script type="text/javascript">
	!function($) {
		$(document).ready(function () {
			<?php if ($response->refresh): ?>
				doDataRefresh();
				setTimeout(getDataSyncStatus, 3000);
			<?php else: ?>
				getDataSyncStatus();
			<?php endif; ?>
		});

		function getDataSyncStatus() {
			$.getJSON(
				<?php echo json_encode($response->statusUrl); ?>,
				function(resp){
					$('#syncStatusMsg').html(resp.msg);

					if('undefined' !== typeof resp.process && resp.process) {
						setTimeout(getDataSyncStatus, 3000);
					}
				}
			);
		}

		<?php if ($response->refresh): ?>
			function doDataRefresh() {
				$.get(
					<?php echo json_encode($response->refreshUrl); ?>
				);
			}
		<?php endif; ?>
	}(jQuery);
</script>


<section id="content">
	<div class="card">
		<div class="card__header">
			<h2>Refreshing User Data</h2>
		</div>


		<div class="card__body">
			<div id="syncStatusMsg">
				<?php echo $response->msg; ?>
			</div>
		</div>
	</div>
</section>
