<div class="row">
    <div class="col-sm-4 col-md-5">
        <div class="text-center pt200"> 
            <img src="<?php echo base_url(); ?>assets/themes/default/images/dayaccountlogo.png" alt="Day Account">
            <span class="txtb txtb2">Track, update, and manage your translations</span>
            <span class="txta txta2">Easier. Faster.</span>
            <hr>
            <div class="text-center">
            	Existing user? <?php echo anchor('users/login', 'Login here', 'class="btn btn-success btn-lg"'); ?>
            </div>
        </div>
    </div>
    <div class="col-sm-8 col-md-7">
        <div class="padtop25">
            <div class="mdcard">
                <h1>Signup<small> for DayTranslations account</small></h1>
                <?php
					if (validation_errors()) {   
						echo '<div id="validation_errors" title="Error:">';
							echo '<ul>';
							echo validation_errors();
							echo '</ul>';
						echo '</div>';
					}
                ?>
                <p class="text-right">        <small><span class="text-danger">*</span> marked fields are mandatory.</small> </p>
                <div id="result"></div>
                                
                <form id="wp_signup_form" action="<?php echo site_url('users/register');?>" method="post" class="form-horizontal" name="wp_signup_form" novalidate>
                    <div class="form-group">
                        <label for="uName" class="col-sm-4 control-label">Username: <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        	<input type="text" name="username" class="form-control selectBoxWithTip" data-rule-required="true" value="<?php echo set_value('username'); ?>" id="uName" aria-required="true">
                        	<span class="help-block"><small>Between 6 to 20 characters (A-Z, a-z), number and _ are allowed to be used.</small></span>
                        </div> 
                    </div>
                    <div class="form-group">
                    	<label class="col-sm-4 control-label" for="email">Email address: <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        	<input type="email" name="email" id="email" class="form-control" data-rule-required="true" data-rule-email="true" value="<?php echo set_value('email'); ?>" aria-required="true">
                        </div>
                    </div>
                    <div class="form-group">
                    	<label class="col-sm-4 control-label" for="password">Password: <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        	<input type="password" name="password" id="password" class="form-control" data-rule-required="true" value="" aria-required="true">
                        </div>
                    </div>
                    <div class="form-group">
                    	<label class="col-sm-4 control-label" for="confirm">Confirm Password: <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        	<input type="password" name="confirm" id="confirm" class="form-control" data-rule-required="true" value="" aria-required="true">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-sm-4 control-label">Name:</label>
                        <div class="col-sm-4">
                            <label class="sr-only" for="first_name"> </label>
                            <input type="text" name="first_name" id="first_name" value="<?php echo set_value('first_name'); ?>" class="form-control" placeholder="First name">
                        </div>
                        <div class="col-sm-4">
                            <label class="sr-only" for="last_name"> </label>
                            <input type="text" name="last_name" id="last_name" value="<?php echo set_value('last_name'); ?>" class="form-control" placeholder="Last name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="company">Company:</label>
                        <div class="col-sm-8">
                        	<input type="text" name="company" id="company" class="form-control" value="<?php echo set_value('company'); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="phone">Phone:</label>
                        <div class="col-sm-8">
                        	<input type="text" name="phone" id="phone" class="form-control" value="<?php echo set_value('phone'); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="address">Address:</label>
                        <div class="col-sm-8">
                        	<input type="text" name="address" id="address" class="form-control" value="<?php echo set_value('address'); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="city">City:</label>
                        <div class="col-sm-8">
                        	<input type="text" name="city" id="city" class="form-control" value="<?php echo set_value('city'); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="state">State:</label>
                        <div class="col-sm-5">
                        	<input type="text" name="state" id="state" class="form-control" value="<?php echo set_value('state'); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="country">Country:</label>
                        <div class="col-sm-5">
                        	<input type="text" name="country" id="country" class="form-control" value="<?php echo set_value('country'); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="zip">Zip:</label>
                        <div class="col-sm-5">
                        	<input type="text" name="zip" id="zip" class="form-control" value="<?php echo set_value('zip'); ?>">
                        	<input type="hidden" name="discount" value="5%">
                        </div>
                    </div>
                    <!--
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="math_captcha"><?php echo $math_captcha_question;?></label>
                        <div class="col-sm-5">
                        	<input type="text" name="math_captcha" id="math_captcha" class="form-control" value="">
                        </div>
                    </div>
                    -->
                    <div class="form-group" style="display:none;">
                        <label class="col-sm-4 control-label sr-only" for="website">Rtest:</label>
                        <div class="col-sm-5">
                        	<input type="text" name="rtest" class="form-control" value="">
                        </div>
                    </div>
                    <div class="form-group text-center">
                        <hr>
                        <input type="submit" id="submitbtn" name="submit" value="Register" class="btn btn-lg  btn-success">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>