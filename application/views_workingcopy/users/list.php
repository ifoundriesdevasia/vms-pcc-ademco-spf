<?php
$colCnt = $isAdmin? 6 : 5;
?>
<style type="text/css">
	.card__header > h2 {
		margin-bottom: 8px;
	}
</style>

<script type="text/javascript">
	function deleteConfirm(url) {
		if (confirm('Do you want to delete this permanently?')) {
			window.location = url;
		}

		return false;
	}
</script>

<section id="content">
    <div class="card">
        <div class="card__header">
            <h2>Staff Table <small>Below are the list of Staff</small></h2>
            <a class="btn btn-default" href="<?php echo site_url('users/add'); ?>">Add New</a>
        </div>


        <div class="card__body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Email</th>
							<th>Phone</th>
							<th>Date</th>

							<?php if ($isAdmin): ?>
								<th>Action</th>
							<?php endif; ?>
						</tr>
                    </thead>

                    <tbody>
						<?php if (empty($userlist)): ?>
							<tr>
								<td colspan="<?php echo $colCnt; ?>"><h3 style="color:red;">No user Found!</h3></td>
							</tr>

						<?php else: ?>
							<?php foreach ($userlist as $user): ?>
								<tr>
									<td><?php echo $user->id; ?></td>
									<td><?php echo $user->name; ?></td>
									<td><?php echo $user->email; ?></td>
									<td><?php echo $user->phone; ?></td>
									<td><?php echo date('d M, Y', strtotime($user->created)); ?></td>

									<?php if ($isAdmin): ?>
										<td>
											<a href="<?php echo site_url('users/update/' . $user->id); ?>" class="btn btn--light btn-xs">Edit</a>&nbsp;&nbsp;

											<!--<a href="#delete"
												class="btn btn--light btn-xs"
												onclick="deleteConfirm('<?php echo site_url('users/delete/' . $user->id); ?>');">Delete</a>-->
										</td>
									<?php endif; ?>
								</tr>
							<?php endforeach; ?>
						<?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
