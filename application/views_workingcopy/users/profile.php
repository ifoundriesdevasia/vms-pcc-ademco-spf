<section id="content">
    <div class="card">
        <div class="card__header">
            <h2>Profle Update</h2>
        </div>

        <div class="card__body">
			<form id="group_add" action="<?php echo site_url('users/profile'); ?>" method="post" >
				<div class="row">
					<?php if ($this->session->flashdata('success')) { ?>
						<div class="alert alert-success">
							<a href="#" class="close" data-dismiss="alert">&times;</a>
							<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
						</div>
					<?php } else if ($this->session->flashdata('error')) { ?>
						<div class="alert alert-danger">
							<a href="#" class="close" data-dismiss="alert">&times;</a>
							<strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
						</div>
					<?php } else if ($this->session->flashdata('warning')) { ?>
						<div class="alert alert-warning">
							<a href="#" class="close" data-dismiss="alert">&times;</a>
							<strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>
						</div>
					<?php } else if ($this->session->flashdata('info')) { ?>
						<div class="alert alert-info">
							<a href="#" class="close" data-dismiss="alert">&times;</a>
							<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
						</div>
					<?php } ?>


					<?php if (validation_errors()): ?>
						<div class="col-md-12">
							<div id="validation_errors" title="Error:">
								<?php echo validation_errors(); ?>
							</div>
						</div>
					<?php endif; ?>


                    <div class="input-group col-sm-12">
                        <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>

                        <div class="form-group">
                            <input type="text" name="name" id="name" class="form-control" value="<?php echo $result->name; ?>" />
							<i class="form-group__bar"></i>
                        </div>
                    </div>

                    <div class="input-group col-sm-12">
                        <span class="input-group-addon"><i class="zmdi zmdi-local-phone"></i></span>

                        <div class="form-group">
                            <input type="text" name="phone" id="phone" class="form-control" value="<?php echo $result->phone; ?>" />
							<i class="form-group__bar"></i>
                        </div>
                    </div>

					<br/>
                    <button class="btn btn-default">Update</button>
				</div>
			</form>
        </div>
    </div>
</section>
