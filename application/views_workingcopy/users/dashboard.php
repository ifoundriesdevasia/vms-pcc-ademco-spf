<div class="content__header content__header--calendar">
    <h2>Calendar</h2>

    <div class="actions actions--calendar">
        <a href="" class="actions__calender-prev"><i class="zmdi zmdi-long-arrow-left"></i></a>
        <a href="" class="actions__calender-next"><i class="zmdi zmdi-long-arrow-right"></i></a>
        <a href="" data-calendar-view="month"><i class="zmdi zmdi-view-comfy active"></i></a>
        <a href="" data-calendar-view="basicWeek"><i class="zmdi zmdi-view-week"></i></a>
        <a href="" data-calendar-view="basicDay"><i class="zmdi zmdi-view-day"></i></a>
    </div>
</div>

<div id="calendar" class="card"></div>