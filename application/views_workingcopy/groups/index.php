<style type="text/css">
	.card__header > h2 {
		margin-bottom: 8px;
	}

	.pagination {
		float: right;
		margin-top: 0;
	}
</style>

<section id="content">
	<div class="card">
		<div class="card__header">
			<h2>Groups Table <small>Below are the list of groups</small></h2>

			<a href="<?php echo site_url('gx-users/refresh/'); ?>"
				class="btn btn-default"
				title="Flush old data then retrieve new set of users data"
				>Refresh Users</a>
		</div>


		<div class="card__body">
			<?php if ($this->session->flashdata('success')) { ?>
				<div class="alert alert-success">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
				</div>
			<?php } else if ($this->session->flashdata('error')) { ?>
				<div class="alert alert-danger">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
				</div>
			<?php } else if ($this->session->flashdata('warning')) { ?>
				<div class="alert alert-warning">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>
				</div>
			<?php } else if ($this->session->flashdata('info')) { ?>
				<div class="alert alert-info">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
				</div>
			<?php } ?>


			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Created By</th>
							<th>Escort</th>
							<th>Visitor</th>
							<th>Date</th>
							<th>Time</th>
							<th>Action</th>
						</tr>
					</thead>


					<tbody>
						<?php if (empty($results)): ?>
							<tr>
								<td colspan="6"><h3 style="color:red;">No user Found!</h3></td>
							</tr>
						<?php else: ?>
							<?php foreach ($results as $result): ?>
								<tr>
									<td><?php echo $result->id; ?></td>
									<td><a href="<?php echo site_url('group-users/reassing/' . $result->id); ?>"><?php echo $result->group_name; ?></a></td>
									<td><?php echo $result->name; ?></td>
									<td>
										<span data-type="group-tap-count" data-taptype="escort" data-tapid="<?php echo $result->id; ?>"><?php echo $result->escort_in_count; ?></span>
									</td>
									<td>
										<span data-type="group-tap-count" data-taptype="visitor" data-tapid="<?php echo $result->id; ?>"><?php echo $result->visitor_in_count; ?></span>
									</td>
									<td><?php echo date('m/d/Y', strtotime($result->created_time)); ?></td>
									<td><?php echo date('H:i s', strtotime($result->created_time)); ?></td>
									<td>
										<ul class="list-inline">
											<li><a href="<?php echo site_url('groups/update/' . $result->id); ?>" class="btn btn--light btn-xs"><i class="zmdi zmdi-edit"></i> Edit</a></li>

											<?php if (FALSE): ?>
											<li><a href="<?php echo site_url('groups/delete/' . $result->id); ?>" class="btn btn--light btn-xs">Delete</a></li>
											<?php endif; ?>

											<li><a href="<?php echo site_url('group-users/reassing/' . $result->id); ?>" class="btn btn--light btn-xs"><i class="zmdi zmdi-accounts"></i> Assign User</a></li>
											<li><a href="<?php echo site_url('groups/resetUsers/' . $result->id); ?>" class="btn btn--light btn-xs"><i class="zmdi zmdi-refresh"></i> Reset</a></li>

											<?php if (FALSE): ?>
											<li><a href="<?php echo site_url('group-users/getcsv/' . $result->id); ?>" class="btn btn--light btn-xs"><i class="zmdi zmdi-file"></i> Export Report</a></li>
											<?php endif; ?>
										</ul>
									</td>
								</tr>
							<?php endforeach; ?>
						<?php endif; ?>
					</tbody>
				</table>


				<?php if (!empty($pagelinks)): ?>
					<?php echo $pagelinks; ?>
					<div class="clearfix"></div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>


<?php if (!empty($results)): ?>
<script type="text/javascript">
	!function($){
		$(document).ready(function(){
			var $wrapper = $('#content');

			// get ids
			var $ids = $wrapper.find('[data-type="group-tap-count"][data-tapid]'), ids = [];
			$ids.each(function(){
				ids.push($(this).data('tapid'));
			});

			if(ids.length < 1) {
				return;
			}

			window.tapids = ids;

			setTimeout(refreshGroupsTapCount, getRefreshTime());

			function refreshGroupsTapCount() {
				$.getJSON(
					<?php echo json_encode($groupInfoUrl); ?>,
					{id: window.tapids},
					function(resp){
						if('undefined' === typeof resp.items || !resp.items.length) {
							return;
						}

						for(var i in resp.items) {
							var item = resp.items[i];

							$wrapper.find('[data-tapid="'+item.id+'"]').each(function(){
								var $this = $(this);
								var num = ('escort' === $this.data('taptype'))?
									item.escort_in_count : item.visitor_in_count;

								$this.hide().html(num).fadeIn();
							});
						}

						setTimeout(refreshGroupsTapCount, getRefreshTime());
					}
				);
			}

			function getRefreshTime() {
				var min = 5, max = 10;
				return (Math.random() * (max - min) + min) * 1000;
			}
		});
	}(jQuery);
</script>
<?php endif; ?>
