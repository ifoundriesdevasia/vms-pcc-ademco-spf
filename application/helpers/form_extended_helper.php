<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeIgniter Form Extended Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		Wesley
 */
// ------------------------------------------------------------------------

if (!function_exists('form_radios')) {
	/**
	 * Radio Buttons
	 *
	 * @param string $data
	 * @param array $options
	 * @param mixed $default
	 * @param mixed $extra
	 * @return string
	 */
	function form_radios($data, $options = array(), $default = null, $extra = array()) {
		is_array($data) OR $data = array('name' => $data);
		$data['type'] = 'radio';

		$id = !empty($data['id'])? $data['id'] : preg_replace('#\W#', '_', $data['name']);
		$class = !empty($data['class'])? $data['class'] : '';
		$lblClass = !empty($extra['labelclass'])? $extra['labelclass'] : '';

		// remove id
		unset($data['id'], $extra['labelclass']);

		$html = '<fieldset id="'.$id.'" class="'.trim('radios ' . $class).'">';

		for($i=0; $i<count($options); $i++) {
			$data['id'] = "{$id}{$i}";

			$value = $options[$i];
			if(is_array($value)) {
				$text = current($value);
				$value = key($value);
			}
			else {
				$text = $value;
			}

			$checked = false;
			if(!is_null($default) && $default == $value) {
				$checked = true;
			}

			$html .= '<label for="'.$data['id'].'" class="'.$lblClass.'">';

			$html .= form_checkbox($data, $value, $checked, $extra);

			$html .= $text;

			$html .= '<i class="input-helper"></i>';

			$html .= '</label>';
		}

		$html .= '</fieldset>';

		return $html;
	}

}


// ------------------------------------------------------------------------

if (!function_exists('form_checkboxes')) {
	/**
	 * Checkbox Fields
	 *
	 * @param string $data
	 * @param array $options
	 * @param mixed $default
	 * @param mixed $extra
	 * @return string
	 */
	function form_checkboxes($data, $options = array(), $default = null, $extra = array()) {
		is_array($data) OR $data = array('name' => $data);
		$data['type'] = 'checkbox';

		$id = !empty($data['id'])? $data['id'] : preg_replace('#\W#', '_', $data['name']);
		$class = !empty($data['class'])? $data['class'] : '';
		$lblClass = !empty($extra['labelclass'])? $extra['labelclass'] : '';

		// remove id
		unset($data['id'], $extra['labelclass']);

		$html = '<fieldset id="'.$id.'" class="checkboxes '.$class.'">';

		for($i=0; $i<count($options); $i++) {
			$data['id'] = "{$id}{$i}";

			$value = $options[$i];
			if(is_array($value)) {
				$text = current($value);
				$value = key($value);
			}
			else {
				$text = $value;
			}

			$checked = false;
			if(!is_null($default) && $default == $value) {
				$checked = true;
			}

			$html .= '<label for="'.$data['id'].'" class="'.trim('checkbox ' . $lblClass).'">';

			$html .= form_checkbox($data, $value, $checked, $extra);

			$html .= $text;

			$html .= '</label>';
		}

		$html .= '</fieldset>';

		return $html;
	}

}
