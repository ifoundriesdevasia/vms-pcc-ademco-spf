<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Actionlog_model Class
 */
class Actionlog_model extends CI_Model {
	protected $table;
	protected $table_actionlogs = 'action_logs';
	protected $table_users = 'users';

	public function __construct() {
		parent::__construct();

		$this->table = $this->table_actionlogs;
	}

	public function getEntry($id) {
		$res = $this->db->select('a.*')
			->from($this->table . ' AS a')
			->where('log_id', (int) $id)
			->limit(1)
			->get('');

		$row = $res->row();

		if(!empty($row->log_id)) {
			return $this->processEntryData($row);
		}

		return false;
	}

	public function getList($params = array(), $limit=null, $offset=null) {
		$db = $this->getListQuery($params);

		$res = $db->select('al.*, u.name AS u_name')
			->order_by('al.log_created DESC')
			->get('', $limit, $offset);

		$rows = array();
		foreach($res->result() as $row) {
			$rows[] = $this->processEntryData($row);
		}

		return $rows;
	}

	public function getListTotal($params = array()) {
		$db = $this->getListQuery($params);

		$res = $db->select('COUNT(*) AS cnt')
			->get('');

		$row = $res->row();

		return empty($row->cnt)? 0 : $row->cnt;
	}

	public function addEntry($action, $targetId, $userId=null, $data = null, $dataOld=null) {
		$userName = '';
		$user = WHelper::getUser($userId);
		if(!empty($user)) {
			$userId = $user->id;
			$userName = $user->name;
		}

		$d = array(
			'log_id'	=> time() . '-' . WHelper::getRandValue(10),

			'log_action'	=> $action,
			'log_target'	=> $targetId? $targetId : 0,
			'log_data'	=> (!is_null($data))? json_encode($data) : '{}',
			'log_data_old'	=> (!is_null($dataOld))? json_encode($dataOld) : '{}',
			'log_ip'	=> WHelper::getUserIP(),

			'log_created'	=> date('Y-m-d H:i:s'),
			'log_created_by'	=> $userId,
			'log_user_name'	=> $userName,
		);

		return $this->db->insert($this->table, $d);
	}

	public function deleteEntry($id) {
		if (!is_array($id)) {
			$id = array($id);
		}

		foreach($id as $_id) {
			$_id = (int) $_id;
			if($_id < 1) {
				continue;
			}

			// delete entry
			$this->db->where('log_id', $id);
			if(!$this->db->delete($this->table)) {
				return false;
			}
		}

		return true;
	}

	public function getCsvDataTotal($params = array()) {
		return $this->getListTotal($params);
	}

	public function getCsvData($params = array(), $limit=null, $offset=null) {
		$db = $this->getListQuery($params);

		$res = $db->select('al.*, u.name AS u_name')
			->order_by('al.log_created DESC')
			->get('', $limit, $offset);

		return $res->result();
	}

	public function processEntryData($row, $isHtml=true) {
		$row->log_data = json_decode($row->log_data, true);
		$row->log_data_old = json_decode($row->log_data_old, true);
		
		$role = '';
		if(isset($row->log_data['fields'])){
			if($row->log_data['fields']['isSuper']=='0'){
				$role = 'Operator';
			}
			else if($row->log_data['fields']['isSuper']=='1'){
				$role = 'Super User';
			}
			else if($row->log_data['fields']['isSuper']=='2'){
				$role = 'Admin';
			}
			$row->log_data['fields']['Role'] = $role;
			unset($row->log_data['fields']['isSuper']);
		}
		$role = '';
		if(isset($row->log_data['isSuper'])){
			if($row->log_data['isSuper']=='0'){
				$role = 'Operator';
			}
			else if($row->log_data['isSuper']=='1'){
				$role = 'Super User';
			}
			else if($row->log_data['isSuper']=='2'){
				$role = 'Admin';
			}
			$row->log_data['Role'] = $role;
			unset($row->log_data['isSuper']);
		}
		$role = '';
		if(isset($row->log_data_old['isSuper'])){
			if($row->log_data_old['isSuper']=='0'){
				$role = 'Operator';
			}
			else if($row->log_data_old['isSuper']=='1'){
				$role = 'Super User';
			}
			else if($row->log_data_old['isSuper']=='2'){
				$role = 'Admin';
			}
			$row->log_data_old['Role'] = $role;
			unset($row->log_data_old['isSuper']);
		}

		$args = array();
		switch($row->log_action) {
			case 'logs.export':
				$format = 'Export logs report';
				break;




			case 'user.login':
				$format = 'Login';
				break;

			case 'user.logout':
				$format = 'Logout';
				break;

			case 'user.add':
				$format = 'Add staff: %s';
				$args[] = (!empty($row->log_data['name']))? $row->log_data['name'] : '';
				break;

			case 'user.edit':
				if(!empty($row->log_data_old)) {
					$txt = '';
					foreach($row->log_data['fields'] as $k => $v) {
						if('password' == $k) {
							$old = $new = '[hidden]';
						}
						else {
							$old = ('' == $row->log_data_old[$k])? '[empty]' : $row->log_data_old[$k];
							$new = ('' == $row->log_data[$k])? '[empty]' : $row->log_data[$k];
						}

						$txt .= $this->formatTxtOld2New($k, $old, $new);
					}

					$format = "Update staff:%s";
					$args[] = $txt;
				}
				else {
					$format = 'Update staff: %s';
					$args[] = (!empty($row->log_data['name']))? $row->log_data['name'] : '';
				}
				break;

			case 'user.delete':
				$format = 'Delete staff: %s';
				$args[] = (!empty($row->log_data['name']))? $row->log_data['name'] : '';
				break;




			case 'group.add':
				$format = 'Add group: %s';
				$args[] = (!empty($row->log_data['group_name']))? $row->log_data['group_name'] : '';
				break;

			case 'group.edit':
				if(!empty($row->log_data_old)) {
					$txt = '';
					foreach($row->log_data['fields'] as $k => $v) {
						$old = ('' == $row->log_data_old[$k])? '[empty]' : $row->log_data_old[$k];
						$new = ('' == $row->log_data[$k])? '[empty]' : $row->log_data[$k];
						$txt .= $this->formatTxtOld2New($k, $old, $new);
					}

					$format = "Update group:%s";
					$args[] = $txt;
				}
				else {
					$format = 'Update group: %s';
					$args[] = (!empty($row->log_data['group_name']))? $row->log_data['group_name'] : '';
				}
				break;

			case 'group.delete':
				$format = 'Delete group: %s';
				$args[] = (!empty($row->log_data['group_name']))? $row->log_data['group_name'] : '';
				break;

			case 'groups.export':
				$format = 'Export group report';
				break;

			case 'group.reset.tap':
				$txt = '';
				$format = 'Reset tap count of group: %s %s';

				$val = (!empty($row->log_data['escort_in_count']))? $row->log_data['escort_in_count'] : 0;
				$txt .= $this->formatTxtValue('Escort IN', $val);

				$val = (!empty($row->log_data['visitor_in_count']))? $row->log_data['visitor_in_count'] : 0;
				$txt .= $this->formatTxtValue('Visitor IN', $val);

				$args[] = (!empty($row->log_data['group_name']))? $row->log_data['group_name'] : '';
				$args[] = $txt;
				break;

			case 'group.assign.user':
				$format = 'Assign %s to group';
				$args[] = (!empty($row->log_data['name']))? $row->log_data['name'] : '';

				// with group data
				if(!empty($row->log_data['group'])) {
					$group = isset($row->log_data['group'][0])? $row->log_data['group'][0] : $row->log_data['group'];
					$format = 'Assign %s to %s';
					$args[] = $group['group_name'];
				}
				break;

			case 'group.dissociate.user':
				$format = 'Unassign %s from group';
				$args[] = (!empty($row->log_data['name']))? $row->log_data['name'] : '';

				// with group data
				if(!empty($row->log_data['group']['group_name'])) {
					$group = isset($row->log_data['group'][0])? $row->log_data['group'][0] : $row->log_data['group'];
					$format = 'Unassign %s from %s';
					$args[] = $group['group_name'];
				}
				break;




			case 'protegegx.refreshusers':
				$format = 'Refresh user data';
				break;

			case 'protegegx.refreshusers.end':
				$format = 'Refresh user data ended';
				break;

			default:
				if(0 === strpos($row->log_action, 'protegegx.api.')) {
					$format = 'Make call with api: %s';
					$args[] = substr($row->log_action, 14);
				}
				else {
					$format = 'Perform %s';
					$args[] = $row->log_action;
				}
		}

		$row->action_msg = str_replace('__NL__', $isHtml? '<br/>' : "\n", vsprintf($format, $args));

		return $row;
	}

	protected function formatTxtOld2New($name, $old, $new) {
		return vsprintf('__NL__%s: %s -> %s', array(
			$name, $old, $new,
		));
	}

	protected function formatTxtValue($name, $value) {
		return vsprintf('__NL__%s: %s', array(
			$name, $value,
		));
	}

	/**
	 * Get list query (basic)
	 * @param array $params Options for retrieving dataset
	 * @return CI_DB
	 */
	protected function getListQuery($params = array()) {
		$db = clone $this->db;

		//
		if(!is_array($params)) {
			$params = (array) $params;
		}

		$db->from($this->table . ' AS al')
			->join($this->table_users . ' AS u', 'u.id = al.log_created_by', 'LEFT');

		// date range
		if(isset($params['date_range']) && '' != $params['date_range']) {
			switch ($params['date_range']) {
				case 'today':
					$dt1 = new DateTime('now');
					$dt2 = new DateTime('now');

					$dt1->setTime(0, 0, 0);
					$dt2->setTime(23, 59, 59);
					break;

				case 'yesterday':
					$dt1 = new DateTime('now');
					$dt1->modify('-1 day');
					$dt1->setTime(0, 0, 0);

					$dt2 = clone $dt1;
					$dt2->setTime(23, 59, 59);
					break;

				case '3dago':
					$dt1 = new DateTime('now');
					$dt1->modify('-3 day');
					$dt1->setTime(0, 0, 0);

					$dt2 = new DateTime('now');
					$dt2->setTime(23, 59, 59);
					break;

				case '1wago':
					$dt1 = new DateTime('now');
					$dt1->modify('-1 week');
					$dt1->setTime(0, 0, 0);

					$dt2 = new DateTime('now');
					$dt2->setTime(23, 59, 59);
					break;

				default:
					// fetch data with user-defined date range
					// if one of the input is invalid, then alltime will be used
					$dates = explode('_', $params['date_range']);
					if(2 === count($dates)) {
						try {
							$dt1 = new DateTime($dates[0]);
							$dt2 = new DateTime($dates[1]);

							$dt1->setTime(0, 0, 0);
							$dt2->setTime(23, 59, 59);
						}
						catch (Exception $ex) {
							unset($dt1, $dt2);
						}
					}
					break;
			}

			if(isset($dt1)) {
				$db->where('al.log_created >=', $dt1->format('Y-m-d H:i:s'))
					->where('al.log_created <=', $dt2->format('Y-m-d H:i:s'));
			}
		}

		return $db;
	}

}
