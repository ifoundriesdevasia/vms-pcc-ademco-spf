<?php

class User extends CI_Model {

	protected $table;

	protected $table_groups = 'groups';

	protected $table_group_users = 'group_users';

	protected $table_latest_event = 'latest_event';

	protected $table_users = 'users';



	public function __construct() {

		parent::__construct();



		$this->table = $this->table_users;

	}



	public function insert_entry($data = NULL) {

		$r = $this->db->insert($this->table, $data);



		// add action log

		if($r) {

			$id = $this->db->insert_id();

			$entry = $this->getUser($id);

			$entry->fields = $data;



//			ActionLog::getInstance()->logUserAdd($r? $this->db->insert_id() : 0, $data);

			ActionLog::getInstance()->logUserAdd($id, $entry);

		}



		return $r;

	}



	public function update_entry($data = NULL, $id = NULL) {

		// data before update

		$entryOld = $this->getUser($id);

		if(empty($entryOld)) {

			return false;

		}



		// update

		$r = $this->db->update($this->table, array_merge($data, array('modified' => date('Y-m-d H:i:s'))), array('id' => $id));



		// add action log

		$entry = clone $entryOld;

		WHelper::objectBindData($entry, $data); // bind data as New data

		$entry->fields = $data;

		ActionLog::getInstance()->logUserEdit($id, $entry, null, $entryOld);



		return $r;

	}



	public function update_event($data = NULL, $id = 1) {

		$this->db->update($this->table_latest_event, $data, array('id' => (int) $id));

	}



	public function user_delete($id) {

		$id = (int) $id;



		// add action log

		$entry = $this->getUser($id);

		if($entry) {

			ActionLog::getInstance()->logUserDelete($id, $entry);

		}



		$this->db->where('id', $id);

		return $this->db->delete($this->table_users);

	}



	public function hasEvent($userId, $door1, $door2) {

		$res = $this->db->select('COUNT(*) AS cnt')

			->from($this->table_latest_event)

			->where('user_id', $userId)

			->where('DoorName', $door1)

			->where('DoorName2', $door2)

			->limit(1)

			->get();



		$row = $res->row();



		return !empty($row->cnt)? $row->cnt > 0 : false;

	}



	public function checkepass($pass = NULL, $id = NULL) {

		$this->db->select('*');

		$this->db->from($this->table);

		$this->db->where('id', $id)

			->where('password', $pass);

		$query = $this->db->get();

		//echo $this->db->last_query(); die;

		return ($this->db->affected_rows() != 1) ? false : true;

	}



	/**

	 * @param int $id

	 * @return object

	 */

	public function getUser($id) {

		if(empty($id) || !is_numeric($id)) {

			return false;

		}



		$row = $this->userlist($id);

		return !empty($row[0])? $row[0] : false;

	}



	/**

	 * @param int $id

	 * @return array An array contain a list of object

	 */

	public function userlist($id = null, $isSuper = []) {

		$this->db->select($this->table_users.'.*,roles.name as role_name');

		$this->db->from($this->table_users);

		if ($id) {

			$this->db->where($this->table_users.'.id', $id);

		}
		if(count($isSuper)>0){
			$this->db->where_in('isSuper', $isSuper);
		}
		$this->db->join('roles', 'roles.role_id = '.$this->table_users.'.isSuper', 'left');
		$query = $this->db->get();



		//echo $this->db->last_query(); die;

		return ($query->num_rows() > 0)? $query->result() : false;

	}



	public function login($data) {

		$query = $this->db->select('*')

			->from($this->table)

			->where('email', $data['email'])

			->where('password', $data['password'])

			->limit(1)

			->get();

		return ($query->num_rows() == 1);

	}



//	public function exists($email) {

//		return ($this->email_exists($email) > 0);

//	}



	public function read_user_information($email) {

		$this->db->select('*');

		$this->db->from($this->table);

		$this->db->where('email', $email);

		$this->db->limit(1);

		$query = $this->db->get();

		return ($query->num_rows() == 1)? $query->result() : false;

	}



	public function email_exists($email) {

		$this->db->select('email');

		$this->db->from($this->table);

		$this->db->where('email', $email);

		$this->db->limit(1);

		$query = $this->db->get();

		return $query->num_rows();

	}

	public function email_edit_exists($email,$uid) {

		$this->db->select('email');

		$this->db->from($this->table);

		$this->db->where('email', $email);
		$this->db->where('id !=', $uid);

		$this->db->limit(1);

		$query = $this->db->get();

		return $query->num_rows();

	}



	public function user_exists($username) {

		$this->db->select('username');

		$this->db->from($this->table);

		$this->db->where('username', $username);

		$this->db->limit(1);

		$query = $this->db->get();

		return $query->num_rows();

	}



	public function resetAllUserByGroup($id = NULL) {

		$data = array('has_tap' => 0);

		return $this->db->update($this->table_group_users, $data, array('group_id' => $id));

	}



}

