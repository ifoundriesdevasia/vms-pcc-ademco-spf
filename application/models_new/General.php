<?php

/**
 *  This Model design all the Genral Functions
 *> Author: Atul Dhanawat
 *> Date:  
* Class General 
*/

class General extends CI_Model {

 	/**
	 * Class Constructor
	 */

	function __construct(){
        // Call the Model constructor
        parent::__construct();
    }	

	/* This will insert the the @data into @tabla
	 * @data 
	 * @tableName
	 */	 
	function addData($tableName=NULL,$data=NULL){
		if((isset($tableName) && isset($data))){
			$this->db->insert($tableName,$data);
			$id=$this->db->insert_id();
			return $id;
		}
	}	

 	/**
	* This will use for custom sql query,mostly that will use for Select Command but we can use insert
	* and update also it just take valid @query string of sql
	*/	

	function findByQuery($query){
		if (isset($query)&& trim($query)!=''){		//@argument Contains value Or Not
			$this->db->trans_start(); 	//start database transction
			$object = $this->db->query($query);
			$this->db->trans_complete(); //complete database transction	
			if ($this->db->trans_status() === FALSE){ //it returns false if transction falied
				$this->db->trans_rollback();			//Rollback to previous state
			}else{
				$this->db->trans_commit();			//either  Commit data
			}
		}
		return $object->result();
	}

	/**
	 * This will update the table ,accourding to @condtions ,,
	 * @tableName ,$condition,$data are required
	 * @conditions are Array
	 */	 

	function updateData($tableName,$condition=NULL,$data){
		if(!isset($condition) && count($condition)>0){
			return FALSE;
		}else{
			if(isset($tableName) && trim($tableName)!='' && isset($data) && count($data)>0){
				$this->db->trans_start(); 	//start database transction
				$this->db->where($condition); //this condition only work for AND like name=this and age=this and....so on.. but if you pass string that works custom condtion or pass array works for AND.

				$this->db->update($tableName,$data); 	
				$this->db->trans_complete(); //complete database transction	
				if ($this->db->trans_status() === FALSE){ //it returns false if transction falied
					$this->db->trans_rollback();			//Rollback to previous state
				}else{
					$this->db->trans_commit();			//either  Commit data
				}
				$error = $this->db->error();
				return isset($error['code'])?$error['code']:0;
			}
		}
	}

	/**
	 * This will delete the recored from the table 
	 * we use @flage which show data to deleted either soft or hard
	 * @tableName @condtions Are required
	 */

	function deleteData($tableName,$condition="",$flag=SOFT_DELETE){
		if(!isset($condition)){
			return FALSE;
		}else{
			if(isset($tableName) && trim($tableName)!=''){
				//$this->db->trans_start(); //start database transction
				$this->db->where($condition); //this condition only work for AND like name=this and age=this and....so on.. but if you pass string that works custom condtion or pass array works for AND.

				if($flag==SOFT_DELETE){
					$this->db->update($tableName,array(IS_DELETED=>TRUE));
				}
				if($flag==HARD_DELETE){
					$this->db->delete($tableName);
					//log_message('error', "DB Error: (".$this->db->_error_number().") ".$this->db->_error_message());
					$error = $this->db->error();
					return isset($error['code'])?$error['code']:0;
				}
			}
		}
	}

	/**
	* Method Name : getFieldValueById 
	*Parameter : $table and $condition and $return_field
	* Task : Loading for getting the field value from the database table according to user_id
	*/

	function getFieldValueById($table_name=NULL,$condition=NULL,$return_field="id"){
		if(!isset($condition)){
			return false;
		}
		$this->db->select('*');
		$this->db->where($condition);
		$recordSet = $this->db->get($table_name);
		$data=$recordSet->result();	

		if(count($data)>0){
			return $data[0]->$return_field;
		}else{
			return false;
		}
	}	

	/**
	* Method Name : isIdVerified 
	*Parameter : $table and $id
	* Task : Loading for verify the id from the table
	*/

	function isIdVerified($table_name=NULL,$id=NULL){
		if($id==null && $table_name==null) return array();
		$this->db->select('*');
		$this->db->where(array('id'=>$id));
		$recordSet = $this->db->get($table_name);
		$data=$recordSet->result();
		if(count($data)>0){
 			return true;
		}else{
			return false;
		}
	}	

}// Class