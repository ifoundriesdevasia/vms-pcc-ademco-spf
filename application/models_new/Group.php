<?php

class Group extends CI_Model {
	protected $table;
	protected $table_groups = 'groups';
	protected $table_group_users = 'group_users';
	protected $table_gx_users = 'gx_users';
	protected $table_users = 'users';

	protected $_cache_data = array();

	public function __construct() {
		parent::__construct();

		$this->table = $this->table_groups;

		// load model
		$this->load->model('group_user', '', true);
	}

	public function insert_data($data = NULL) {
		$data2 = array();
		if(empty($data['created_time'])) {
			$data2['created_time'] = date('Y-m-d H:i:s');
		}

		if(empty($data['created_by'])) {
			$data2['created_by'] = WHelper::getUser()->id;
		}

		$r = $this->db->insert($this->table, array_merge($data, $data2));

		// add action log
		if($r) {
			$id = $this->db->insert_id();

			$entry = $this->getData($id, true);
			$entry = $entry[0];
			$entry->fields = $data;

//			ActionLog::getInstance()->logGroupAdd($r? $this->db->insert_id() : 0, $data);
			ActionLog::getInstance()->logGroupAdd($id, $entry);
		}

		return $r;
	}

	public function update_data($data = NULL, $id) {
		$data2 = array();
		if(empty($data['modified'])) {
			$data2['modified'] = date('Y-m-d H:i:s');
		}

		if(empty($data['modified_by'])) {
			$data2['modified_by'] = WHelper::getUser()->id;
		}

		// data before update
		$entryOld = $this->getData($id, true);
		if(empty($entryOld)) {
			return false;
		}

		$entryOld = $entryOld[0];

		// update
		$r = $this->db->update($this->table, array_merge($data, $data2), array('id' => $id));

		// add action log
		$entry = clone $entryOld;
		WHelper::objectBindData($entry, $data); // bind data as New data
		$entry->fields = $data;
		ActionLog::getInstance()->logGroupEdit($id, $entry, null, $entryOld);

		return $r;
	}

	public function delete($id) {
		// get group data for log
		$data = $this->getData($id);

		$r = $this->db->where('id', $id)
			->delete($this->table);

		if($r) {
			// add action log
			ActionLog::getInstance()->logGroupDelete($id, $data[0]);

			// delete dependancy
			$this->group_user->deleteByGroupID($id);
		}

		return $r;
	}

	/**
	 * Proxy function of getData()
	 * @param int $id
	 * @param bool $refresh
	 * @return array	A list of object
	 */
	public function get_data($id = NULL, $refresh=false) {
		return $this->getData($id, $refresh); // identical function
	}

	/**
	 * @param int $id
	 * @param bool $refresh
	 * @return array	A list of object
	 */
	public function getData($id = NULL, $refresh=false) {
		if(is_null($id)) {
			$id = 'null';
		}

		if(!isset($this->_cache_data[$id]) || $refresh) {
			$params = array();
			if (is_numeric($id) && $id > 0) {
				$params['id'] = $id;
			}

			$rows = $this->getList($params);

			$this->_cache_data[$id] = empty($rows)? false : $rows;
		}

		return $this->_cache_data[$id];
	}

	public function getList($params = array(), $limit=null, $offset=null) {
		$db = $this->getListQuery($params);

		$res = $db->select('g.*, u.name, u.id as userID')
			->order_by('g.id DESC')
			->get('', $limit, $offset);

		return ($res->num_rows() > 0)? $res->result() : array();
	}

	public function getListTotal($params = array()) {
		$db = $this->getListQuery($params);

		$res = $db->select('COUNT(*) AS cnt')
			->get('');

		$row = $res->row();

		return empty($row->cnt)? 0 : $row->cnt;
	}

	public function updateAllTapCount($id) {
		// types (i): 0 => escprt; 1 => visitor
		// tap (j): 0 => out; 1 => in
		$counts = array();
		for($i=0; $i<=1; $i++) {
			for($j=0; $j<=1; $j++) {
				$res = $this->db->select('COUNT(*) AS cnt')
					->from($this->table_group_users . ' AS gu')
					->join($this->table_gx_users . ' AS gxu', 'gxu.user_id = gu.user_id', 'INNER')
					->where('gu.group_id', $id)
					->where('gu.has_tap', $j)
					->where('gxu.cf_visitor', $i)
					->get();

				$row = $res->row();
				$counts[$i][$j] = !empty($row->cnt)? $row->cnt : 0;
			}
		}

		$data = array(
			'escort_out_count'	=> $counts[0][0],
			'escort_in_count'	=> $counts[0][1],
			'visitor_out_count'	=> $counts[1][0],
			'visitor_in_count'	=> $counts[1][1],
		);

		return $this->update_data($data, $id);
	}

	public function getTotalTapIn($id, $refresh=true) {
		$row = $this->getData($id, $refresh);
		return $row? ($row[0]->visitor_in_count + $row[0]->escort_in_count) : 0;
	}

	public function getTotalTapOut($id, $refresh=true) {
		$row = $this->getData($id, $refresh);
		return $row? ($row[0]->visitor_out_count + $row[0]->escort_out_count) : 0;
	}

	public function getTotalTapInVisitor($id, $refresh=true) {
		$row = $this->getData($id, $refresh);
		return $row? $row[0]->visitor_in_count : 0;
	}

	public function getTotalTapOutVisitor($id, $refresh=true) {
		$row = $this->getData($id, $refresh);
		return $row? $row[0]->visitor_out_count : 0;
	}

	public function getTotalTapInEscort($id, $refresh=true) {
		$row = $this->getData($id, $refresh);
		return $row? $row[0]->escort_in_count : 0;
	}

	public function getTotalTapOutEscort($id, $refresh=true) {
		$row = $this->getData($id, $refresh);
		return $row? $row[0]->escort_out_count : 0;
	}

	/**
	 * Get list query (basic)
	 * @param array $params Options for retrieving dataset
	 * @return CI_DB
	 */
	protected function getListQuery($params = array()) {
		$db = clone $this->db;

		$db->from($this->table . ' AS g');

		// group ID
		if (isset($params['id'])) {
			$db->where('g.id', (int) $params['id']);
		}

		$db->join($this->table_users . ' AS u', 'g.created_by = u.id', 'LEFT');

		return $db;
	}

}
