<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_URI extends CI_URI {
	/**
	 * Similar as segment_array() with additional extract data by colon (:) in segment as array key.
	 * e.g page/action/15/data:123 -->
	 * [
	 *		[1]	=> page,
	 *		[2]	=> action,
	 *		[3]	=> 15,
	 *		[4]	=> data:123,
	 *		[data]	=> 123,
	 * ]
	 *
	 * @param bool $routedSegment
	 * @return	array	CI_URI::$segments
	 */
	public function segment_data($routedSegment=false) {
		$r = $routedSegment? $this->rsegments : $this->segments;
		foreach($r as $segment) {
			$pos = strpos($segment, ':');
			if($pos > 0) {
				$k = substr($segment, 0, $pos);
				if('' == $k) {
					continue;
				}
				$r[$k] = substr($segment, $pos + 1);
			}

//			if(strpos($segment, ':') > 0) {
//				$parts = explode(':', $segment);
//				$k = array_shift($parts);
//				$r[$k] = implode('', $parts);
//			}
		}

		return $r;
	}

}
