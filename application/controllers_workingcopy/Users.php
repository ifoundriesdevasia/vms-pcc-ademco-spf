<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//date_default_timezone_set('America/New_York');
error_reporting(0);

class Users extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->output->set_template('default');

		$this->load->model('user', '', true);
		$this->load->model('group', '', true);
		$this->load->model('group_user', '', true);

		if (!$this->session->userdata('logged_in')) {
			// Allow some methods?
			$allowed = array(
				'login', 'updaterecord', 'updaterecordset', 'userupdate',
				'latestevent', 'userlist', 'cronscheduler', 'adduser',
				'showuserlist', 'fulluserdetails', 'updateuserstatus',
				'resetallvisitor', 'resetallescort'
			);

			if (!in_array($this->router->fetch_method(), $allowed)) {
				redirect('users/login');
			}
		}

		$user = $this->session->userdata('logged_in');
		$this->userID = $user['id'];
		$this->isSuper = $user['isSuper'];
//		$this->_init();
	}

//	private function _init() {
//		$this->ci = & get_instance();
//		$this->ci->config->load('ProtegeGX');
//	}

	public function index() {
		$this->output->set_template('default');
//		$this->output->set_title('Singapore Police Force');

		if (!$this->session->userdata('logged_in')) {
			redirect('users/login');
		}

//		$this->load->view('users/login');
		$this->staffs();
	}

	public function staffs() {
		$this->output->set_template('default');

		$user = $this->session->userdata('logged_in');

		$data = array(
			'isAdmin'	=> !empty($user['isSuper']),
			'userlist'	=> $this->user->userlist(),
		);

		$this->load->view('users/list', $data);
	}

	//Regiatration by post and regaster form
	public function add() {
		$this->output->set_template('default');
//		$this->output->set_title('SPF::New User Registartion');
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_isEmailExist');
		//$this->form_validation->set_rules('username','Username','trim|required|min_length[6]|callback_isUserExist');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('confirm', 'Confirm Password', 'required|matches[password]');
		//$this->form_validation->set_rules('phone','Phone','regex_match[/^[0-9]{10}$/]');

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('users/add');
		}
		else {
			$data = array(
				'name' => $this->input->post('name', TRUE),
				'email' => $this->input->post('email', TRUE),
				//'username' => $this->input->post('username', TRUE),
				'password' => do_hash($this->input->post('password', TRUE), 'md5'),
				'phone' => $this->input->post('phone', TRUE),
				'isSuper' => 1,
				'created' => date('Y-m-d H:i:s'),
				'modified' => date('Y-m-d H:i:s')
			);

			//check user already in CI DB
			$have_user = $this->user->email_exists($data['email']);

			//Save registration date in CodeIgniter
			if (!$have_user) {
				$data['created'] = date('Y-m-d H:i:s');
				$result = $this->user->insert_entry($data);
				$this->output->set_message('SPF::Staff added Successfully!');
				redirect('users/staffs');
			}

			$this->output->set_message('Username already exist!');
			$this->load->view('users/add');
		}
	}

	public function update() {
		$uid = $this->uri->segment(3);

		$this->output->set_template('default');
//		$this->output->set_title('SPF::Staff Update');
		$this->form_validation->set_rules('name', 'Name', 'required');
		//$this->form_validation->set_rules('phone','Phone','regex_match[/^[0-9]{10}$/]');
		if ($this->form_validation->run() == FALSE) {
			// nothing to do
		}
		else {
			$data = array(
				'name' => $this->input->post('name', TRUE),
				'phone' => $this->input->post('phone', TRUE),
//				'modified' => date('Y-m-d H:i:s')
			);

			$result = $this->user->update_entry($data, $uid);

			if ($result == TRUE) {
				$this->output->set_message('SPF::Staff Update Successfully!');
			}
			else {
				$this->output->set_message('Unable to update');
			}
		}

		$userlist = $this->user->userlist($uid);
		$data['result'] = $userlist;

		$this->load->view('users/update', $data);
	}

	public function password() {
		$this->output->set_template('default');
//		$this->output->set_title('SPF::New User Registartion');

		$userDetails = $this->session->userdata('logged_in');
		$uid = $userDetails['id'];

		$this->form_validation->set_rules('old_pass', 'Old Password', 'required|trim|xss_clean|callback_oldpassword_check');
		$this->form_validation->set_rules('new_pass', 'New Password', 'required|trim');
		$this->form_validation->set_rules('confirm_pass', 'Confirm Password', 'required|trim|matches[new_pass]');

		if ($this->form_validation->run() == FALSE) {
			// nothing to do
		}
		else {
			$userdataSave = array(
				'password' => do_hash($this->input->post('new_pass', TRUE), 'md5')
			);

			$updateresult = $this->user->update_entry($userdataSave, $uid);
			if ($updateresult) {
				$this->session->set_flashdata('success', 'Password changed.');
			}
			else {
				$this->session->set_flashdata('error', 'Unable to change password .');
			}
		}

		$this->load->view('users/password');
	}

	public function delete() {
		$uid = (int) $this->uri->segment(3);

		// check if delete self?
		$self = WHelper::getUser();
		if(!empty($self) && (int) $self->id === $uid) {
			$this->session->set_flashdata('error', 'You cannot your own account.');
			redirect('users/staffs');
		}

		$this->user->user_delete($uid);

		redirect('users/staffs');
	}

	public function login() {
		$this->output->set_template('login');
//		$this->output->set_title('SPF::User Login');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			if (isset($this->session->userdata['logged_in'])) {
				//$this->load->view('users/dashboard', $data);
				$this->session->set_flashdata('success', 'You are already login.');
				redirect('groups/index');
			}

			$this->load->view('users/login');
		}
		else {
			$data = array(
				'email' => $this->input->post('email'),
				'password' => do_hash($this->input->post('password', TRUE), 'md5')
			);

			$result = $this->user->login($data);
			if ($result === true) {
				$email = $this->input->post('email');
				$result = $this->user->read_user_information($email);

				if ($result != false) {
					// remove password
					unset($result[0]->password);

					// store all data
					$session_data = (array) $result[0];

					$this->session->set_userdata('logged_in', $session_data);
					$this->session->set_flashdata('success', 'You have successfully login.');

					// add action log
					$this->actionlog->logUserLogin($session_data['id']);

					redirect('groups/index');
				}
			}
			else {
				$this->session->set_flashdata('error', 'Invalid Username or Password');
				$this->load->view('users/login');
			}
		}
	}

	public function logout() {
		$user = WHelper::getUser();
		if(empty($user)) {
			// session expired somehow
			redirect('users/login');
		}

		// add action log
		$this->actionlog->logUserLogout($user->id);

		$sess_array = array(
			'email' => ''
		);
		$this->session->unset_userdata('logged_in', $sess_array);
		$this->session->set_flashdata('success', 'You have successfully logout.');
		redirect('users/login');
	}

	public function dashboard() {
		$this->output->set_template('default');
//		$this->output->set_title('SPF::User Dashboard');
		$this->load->view('users/dashboard');
	}

	public function profile() {
		$this->output->set_template('default');
//		$this->output->set_title('SPF::User Profile');
		$this->form_validation->set_rules('name', 'Name', 'required');

		$user = $this->session->userdata('logged_in');
		$uid = $user['id'];
		$userlist = $this->user->userlist($uid);
		//print_r($userlist); die;
		$data['result'] = $userlist['0'];
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('users/profile', $data);
		} else {
			$records = array(
				'name' => $this->input->post('name', TRUE),
				'phone' => $this->input->post('phone', TRUE),
//				'modified' => date('Y-m-d H:i:s')
			);

			//check user already in CI DB
			$result = $this->user->update_entry($records, $uid);
			$userlist = $this->user->userlist($uid);
			$data['result'] = $userlist['0'];
			if ($result == TRUE) {
				$this->output->set_message('SPF::Update Successfully!');
				$this->load->view('users/profile', $data);
			} else {
				$this->output->set_message('Username already exist!');
				$this->load->view('users/profile', $data);
			}
		}
	}

	public function cronscheduler() {
//		date_default_timezone_set('Asia/Singapore');
		$this->output->set_template('blank');

		// get latest user events
		$resultSet = $this->protegegx->getLatestEventsUsers(1);

		$xml = ($resultSet->GetLatestEventsResult == 1) ? simplexml_load_string($resultSet->strEventReportDataCollection) : false; //Getting data from XML

		// ifoundries add 23 oct 2017
		if ((strtotime($xml->EventReportData->Logged) + 10) <= strtotime(date('Y-m-d H:i:s'))) {
			$tmpdata = array('user_id' => '0');
			$this->user->update_event($tmpdata);
			return false;
		}

		$DoorName = (string) $xml->EventReportData->DoorName;
		$DoorName2 = (string) $xml->EventReportData->DoorName2;

		// get cards' number from user name
		$userFullDetails = $xml->EventReportData->UserName;
		$userFullDetailsArr = explode('(', $userFullDetails);
		$userDetails = end($userFullDetailsArr);
		$CardNumberFamilyNumber = str_replace(')', '', $userDetails);
		$CardNumberFamilyNumberArr = explode(':', $CardNumberFamilyNumber);
		$CardNumber = $CardNumberFamilyNumberArr['1'];
		$FamilyNumber = $CardNumberFamilyNumberArr['0'];

$debug = [
	'xml'	=> $resultSet->strEventReportDataCollection,
];

		// user exists?
		$UserData = $this->group_user->getDataByCardNumber($CardNumber, $FamilyNumber);  // Getting user details form our DB
		if(empty($UserData)) {
			return false;
		}

$debug['userData'] = $UserData;

		$userStatus = $UserData->has_tap;
		$user_id = $UserData->user_id;
		$type = $UserData->cf_visitor;
		$group_id = $UserData->group_id;
		$last_tap = array(
			'user_id' => $user_id,
			'DoorName' => $DoorName,
			'DoorName2' => $DoorName2
		);

$debug['last_tap'] = $last_tap;

//		if ($type == 0 && $escortTapInCount == 1 && $visitorTapInCount > 0 && $userStatus == 1) {
//			return false;
//		}
//
//		if ($type == 1 && $escortTapInCount < 1 && $visitorTapInCount == 0 && $userStatus == 0) {
//			return false;
//		}

		// This is checking is the last event is unique
		// true mean the event has been processed before
		$result = $this->user->hasEvent($user_id, $DoorName, $DoorName2);

$debug['hasEvent'] = $result;
		if($result) {
			return;
		}

$last_tap['debug'] = print_r($debug, 1);

		// update THIS current event
		$this->user->update_event($last_tap);

		// gates set in config
		$gatesIn = $this->config->item('gates_in');
		$gatesOut = $this->config->item('gates_out');

		// tap IN
		if(in_array($DoorName, $gatesIn)) {
			$tapStatus = 1;
		}

		// tap OUT
		elseif(in_array($DoorName, $gatesOut)) {
			$tapStatus = 0;
		}
		else {
			return false; // gate not found
		}

		// update tap status and count
		$this->group_user->setUserTapStatus($group_id, $user_id, $tapStatus);

		// tap IN count
		$escortTapInCount = $this->group->getTotalTapInEscort($group_id); // Getting escort tap-in count belongs to this escort group for our DB
		$visitorTapInCount = $this->group->getTotalTapInVisitor($group_id); // Getting visitor tap-in count belongs to this escort group for our DB

		// escort not in the room
		if($escortTapInCount === 0) {
			// enable all escorts
			$escorts = $this->group_user->getAllEscorts($group_id);
			if(!empty($escorts)) {
				foreach ($escorts as $escort) {
					$result = $this->protegegx->disableUser($escort->user_id, false); // enable
				}
			}

			// disable all visitor
			$visitors = $this->group_user->getAllVisitors($group_id);
			if(!empty($visitors)) {
				foreach ($visitors as $visitor) {
					$result = $this->protegegx->disableUser($visitor->user_id, true); // disable
				}
			}
		}

		// only one escort in the room
		elseif($escortTapInCount === 1) {
			// if there is visitor in room then disable escort in room
			// if no visitor in room then enable all escorts
			if($visitorTapInCount > 0) {
				// disable escorts IN the room
				$escorts = $this->group_user->getAllEscorts($group_id, 1);
				if(!empty($escorts)) {
					foreach ($escorts as $escort) {
						$result = $this->protegegx->disableUser($escort->user_id, true); // enable
					}
				}

				// enable escorts OUTside room
				$escorts = $this->group_user->getAllEscorts($group_id, 0);
				if(!empty($escorts)) {
					foreach ($escorts as $escort) {
						$result = $this->protegegx->disableUser($escort->user_id, false); // enable
					}
				}
			}
			else {
				// enable escorts OUTside room
				$escorts = $this->group_user->getAllEscorts($group_id);
				if(!empty($escorts)) {
					foreach ($escorts as $escort) {
						$result = $this->protegegx->disableUser($escort->user_id, false); // enable
					}
				}
			}

			// enable all visitor
			$visitors = $this->group_user->getAllVisitors($group_id);
			if(!empty($visitors)) {
				foreach ($visitors as $visitor) {
					$result = $this->protegegx->disableUser($visitor->user_id, false); // enable
				}
			}
		}

		// more than 1 escort in the room
		else {
			// escort > 1

			// enable escorts OUTside room
			$escorts = $this->group_user->getAllEscorts($group_id);
			if(!empty($escorts)) {
				foreach ($escorts as $escort) {
					$result = $this->protegegx->disableUser($escort->user_id, false); // enable
				}
			}

			// enable all visitor
			$visitors = $this->group_user->getAllVisitors($group_id);
			if(!empty($visitors)) {
				foreach ($visitors as $visitor) {
					$result = $this->protegegx->disableUser($visitor->user_id, false); // enable
				}
			}
		}




		/*
		// tap in function start here // INCOMING READER
		if ($DoorName == "D-A-04A TURNSTILE" || $DoorName == "D-B-06A TURNSTILE") {
			$setStatus = 1;
			$this->group_user->setUserTapStatus($group_id, $user_id, $setStatus); // This function is for set the user tap-in for our DB

			// escot type tap in
			if ($type == 0) {
				$escortTapInCount = $this->group->getTotalTapInEscort($group_id); // Getting escort tap-in count (refresh)
				$visitorTapInCount = $this->group->getTotalTapInVisitor($group_id); // Getting visitor tap-in count (refresh)

				if ($escortTapInCount == 1) { //If counter is 1 means this one is first escort
					$visitirIDs = $this->group_user->getAllVisitors($group_id); // Getting Visitor belongs to this escort group for our DB
					foreach ($visitirIDs as $visitirID) {  // Make all visitor enable  belongs to this escort group
						$vId = $visitirID->user_id;
						$result = $this->protegegx->disableUser($vId, false); // Enabling visitor
					}
				}
				else if ($escortTapInCount > 1 && $visitorTapInCount > 0) {
					$ecortIDs = $this->group_user->getAllEscorts($group_id, 1);  // Getting Escort belongs to this escort group for our DB
					foreach ($ecortIDs as $ecortID) {  // Make Escort enable  belongs to this visitor group
						$vId = $ecortID->user_id;
						$result = $this->protegegx->disableUser($vId, false); // Enabling Escort
					}
				}
			}

			// Visitor type tap in
			else {
				$escortTapInCount = $this->group->getTotalTapInEscort($group_id); // Getting escort tap-in count (refresh)

				if ($escortTapInCount == 1) { //If counter is 1 means this one escort tap in
					$ecortIDs = $this->group_user->getAllEscorts($group_id, 1);  // Getting Escort belongs to this visitor group for our DB
					foreach ($ecortIDs as $ecortID) {  // Make Escort disable  belongs to this visitor group
						$vId = $ecortID->user_id;
						$result = $this->protegegx->disableUser($vId, true); // disabling Escort
					}
				}
			}
		}


		// tap out function //OUTGOING READER
		else if ($DoorName == "D-A-04B TURNSTILE" || $DoorName == "D-B-06B TURNSTILE") {
			$setStatus = 0;
			$this->group_user->setUserTapStatus($group_id, $user_id, $setStatus); // This function is for set the user tap-out for our DB

			if ($type == 0) { // escot type tap out
//					$escortTapOutCount = $this->group->getTotalTapOutEscort($group_id); // tap OUT
				$escortTapOutCount = $this->group->getTotalTapInEscort($group_id); // still tap IN count
				$visitorTapInCount = $this->group->getTotalTapInVisitor($group_id); // tap IN

				if ($escortTapOutCount == 0) { //Counter 0 means last escort tap out
					// Getting all visitor belogns this escort group
					// no escort in the room, all visitor cannot tap in/out
					$visitirIDs = $this->group_user->getAllVisitors($group_id);
					foreach ($visitirIDs as $visitirID) { // Disable all visitor belongs this escort group
						$vId = $visitirID->user_id;
						$result = $this->protegegx->disableUser($vId, true); // disabling Visitor
					}
				}
				else if ($escortTapOutCount == 1 && $visitorTapInCount > 0) {
					$ecortIDs = $this->group_user->getAllEscorts($group_id, 1);  // Getting Escort belongs to this Escort group for our DB
					foreach ($ecortIDs as $ecortID) {  // Make Escort disable  belongs to this visitor group
						$vId = $ecortID->user_id;
						$result = $this->protegegx->disableUser($vId, true); // disabling Escort
					}
				}
			}
			else { // visitor type tap out
				// tap out count after update tap
//					$escortTapOutCount = $this->group->getTotalTapOutEscort($group_id);
//					$visitorTapOutCount = $this->group->getTotalTapOutVisitor($group_id);
				$escortTapOutCount = $this->group->getTotalTapInEscort($group_id); // still tap IN count
				$visitorTapOutCount = $this->group->getTotalTapInVisitor($group_id); // still tap IN count

				if ($visitorTapOutCount == 0 && $escortTapOutCount == 1) {
					$ecortIDs = $this->group_user->getAllEscorts($group_id, 1);  // Getting Escort belongs to this Escort group for our DB
					foreach ($ecortIDs as $ecortID) {  // Make Escort disable  belongs to this visitor group
						$vId = $ecortID->user_id;
						$result = $this->protegegx->disableUser($vId, false); // enable Escort
					}
				}
			}
		}
		*/
	}

	public function oldpassword_check($old_password) {
		$userDetails = $this->session->userdata('logged_in');

		$uid = $userDetails['id'];
		$pass = do_hash($old_password, 'md5');
		$result = $this->user->checkepass($pass, $uid);

		if (!$result) {
			$this->form_validation->set_message('oldpassword_check', 'Old password not match.');
			return false;
		}

		return true;
	}

	public function isEmailExist($key) {
		$email = $this->input->post('email');
		$num_email = $this->user->email_exists($email);

		if ($num_email > 0) {
			$this->form_validation->set_message('isEmailExist', 'email already exists!');
			return false;
		}

		return true;
	}

	public function isUserExist($key) {
		$username = $this->input->post('username');
		$num_user = $this->user->user_exists($username);

		if ($num_user > 0) {
			$this->form_validation->set_message('isUserExist', 'User already exists!');
			return false;
		}

		return true;
	}

	protected function test() {
//		date_default_timezone_set('Asia/Singapore');
		$this->output->set_template('blank');

		// get latest user events
		$resultSet = $this->protegegx->getLatestEventsUsers(1);

		$xml = ($resultSet->GetLatestEventsResult == 1) ? simplexml_load_string($resultSet->strEventReportDataCollection) : false; //Getting data from XML

		// ifoundries add 23 oct 2017
		if (false && (strtotime($xml->EventReportData->Logged) + 10) <= strtotime(date('Y-m-d H:i:s'))) {
			$tmpdata = array('user_id' => '0');
			$this->user->update_event($tmpdata);
			return false;
		}

		$DoorName = (string) $xml->EventReportData->DoorName;
		$DoorName2 = (string) $xml->EventReportData->DoorName2;

		// get cards' number from user name
		$userFullDetails = $xml->EventReportData->UserName;
		$userFullDetailsArr = explode('(', $userFullDetails);
		$userDetails = end($userFullDetailsArr);
		$CardNumberFamilyNumber = str_replace(')', '', $userDetails);
		$CardNumberFamilyNumberArr = explode(':', $CardNumberFamilyNumber);
		$CardNumber = $CardNumberFamilyNumberArr['1'];
		$FamilyNumber = $CardNumberFamilyNumberArr['0'];

$debug = [
	'xml'	=> $resultSet,
	//'xml'	=> $resultSet->strEventReportDataCollection,
];

		// user exists?
		$UserData = $this->group_user->getDataByCardNumber($CardNumber, $FamilyNumber);  // Getting user details form our DB
		if(empty($UserData)) {
			return false;
		}

$debug['userData'] = $UserData;

		$userStatus = $UserData->has_tap;
		$user_id = $UserData->user_id;
		$type = $UserData->cf_visitor;
		$group_id = $UserData->group_id;
		$last_tap = array(
			'user_id' => $user_id,
			'DoorName' => $DoorName,
			'DoorName2' => $DoorName2
		);

$debug['last_tap'] = $last_tap;

//		if ($type == 0 && $escortTapInCount == 1 && $visitorTapInCount > 0 && $userStatus == 1) {
//			return false;
//		}
//
//		if ($type == 1 && $escortTapInCount < 1 && $visitorTapInCount == 0 && $userStatus == 0) {
//			return false;
//		}

		// This is checking is the last event is unique
		// true mean the event has been processed before
		$result = $this->user->hasEvent($user_id, $DoorName, $DoorName2);

$debug['hasEvent'] = $result;
		if(false && $result) {
WHelper::debug($debug, 'debug');
exit;
			return;
		}

$last_tap['debug'] = print_r($debug, 1);



		// update THIS current event
		$this->user->update_event($last_tap);


		// gates set in config
		$gatesIn = $this->config->item('gates_in');
		$gatesOut = $this->config->item('gates_out');

		// tap IN
		if(in_array($DoorName, $gatesIn)) {
			$tapStatus = 1;
		}

		// tap OUT
		elseif(in_array($DoorName, $gatesOut)) {
			$tapStatus = 0;
		}
		else {
WHelper::debug('DOOR not found!!', 'debug');
exit;
			return false; // gate not found
		}

//WHelper::debug($debug, 'debug');
WHelper::debug(__LINE__, 'LINE');
WHelper::debug($tapStatus, 'tapStatus');
//exit;

		// update tap status and count
		$this->group_user->setUserTapStatus($group_id, $user_id, $tapStatus);

		// tap IN count
		$escortTapInCount = $this->group->getTotalTapInEscort($group_id); // Getting escort tap-in count belongs to this escort group for our DB
		$visitorTapInCount = $this->group->getTotalTapInVisitor($group_id); // Getting visitor tap-in count belongs to this escort group for our DB

WHelper::debug(__LINE__, 'LINE');
WHelper::debug($escortTapInCount, 'escortTapInCount');
WHelper::debug($visitorTapInCount, 'visitorTapInCount');
//exit;

		// escort not in the room
		if($escortTapInCount === 0) {
WHelper::debug(__LINE__, 'LINE');
			// enable all escorts
			$escorts = $this->group_user->getAllEscorts($group_id);
			if(!empty($escorts)) {
				foreach ($escorts as $escort) {
					$result = $this->protegegx->disableUser($escort->user_id, false); // enable
				}
			}

			// disable all visitor
			$visitors = $this->group_user->getAllVisitors($group_id);
			if(!empty($visitors)) {
				foreach ($visitors as $visitor) {
					$result = $this->protegegx->disableUser($visitor->user_id, true); // disable
				}
			}
		}

		// only one escort in the room
		elseif($escortTapInCount === 1) {
WHelper::debug(__LINE__, 'LINE');
			// disable escorts IN the room
			$escorts = $this->group_user->getAllEscorts($group_id, 1);
WHelper::debug($escorts, 'escorts - IN');
			if(!empty($escorts)) {
				foreach ($escorts as $escort) {
					$result = $this->protegegx->disableUser($escort->user_id, true); // enable
				}
			}

			// enable escorts OUTside room
			$escorts = $this->group_user->getAllEscorts($group_id, 0);
WHelper::debug($escorts, 'escorts - OUT');
			if(!empty($escorts)) {
				foreach ($escorts as $escort) {
					$result = $this->protegegx->disableUser($escort->user_id, false); // enable
				}
			}

			// enable all visitor
			$visitors = $this->group_user->getAllVisitors($group_id);
WHelper::debug($visitors, 'visitors - ALL');
			if(!empty($visitors)) {
				foreach ($visitors as $visitor) {
					$result = $this->protegegx->disableUser($visitor->user_id, false); // enable
				}
			}
		}

		// more than 1 escort in the room
		else {
WHelper::debug(__LINE__, 'LINE');
			// escort > 1

			// enable escorts OUTside room
			$escorts = $this->group_user->getAllEscorts($group_id);
			if(!empty($escorts)) {
				foreach ($escorts as $escort) {
					$result = $this->protegegx->disableUser($escort->user_id, false); // enable
				}
			}

			// enable all visitor
			$visitors = $this->group_user->getAllVisitors($group_id);
			if(!empty($visitors)) {
				foreach ($visitors as $visitor) {
					$result = $this->protegegx->disableUser($visitor->user_id, false); // enable
				}
			}
		}

exit;
	}

}
