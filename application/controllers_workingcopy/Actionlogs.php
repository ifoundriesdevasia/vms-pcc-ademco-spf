<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Actionlogs extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->output->set_template('default');

		$this->load->model('actionlog_model', '', TRUE);

		if (!$this->session->userdata('logged_in')) {
			redirect('users/login');
		}
	}

	public function index() {
//		redirect('groups');
		$this->form();
	}

	public function form() {
		$postForm = $this->input->post('form');

		$data = array();
		$params = array();

		// check date range by form values
		if(!empty($postForm)) {
			if(empty($postForm['dateoption'])) {
				$postForm['dateoption'] = 'all'; // all time
			}

			if('custom' == $postForm['dateoption']) {
				$now = new DateTime('now');
				$date1 = (!empty($postForm['date1']))? $postForm['date1'] : $now->format('Y-m-d');
				$date2 = (!empty($postForm['date2']))? $postForm['date2'] : $now->format('Y-m-d');
				$params['date_range'] = "{$date1}_{$date2}";
			}
			else {
				$params['date_range'] = $postForm['dateoption'];
			}

			// store post state
			$params['form'] = $postForm;
		}
		else {
			// possible values:
			// actionlogs/form/today/20
			// actionlogs/form/eyJkYXRlX3JhbmdlIjoidG9kYXkifQ==/20
			$dateRange = $this->uri->segment(3);
			$pageOffset = (int) $this->uri->segment(4);

			// treat it as page offset
			if(is_numeric($dateRange)) {
				$pageOffset = (int) $dateRange;
				$dateRange = '';
			}

			// get data range
			if('' != $dateRange) {
				$tmp = base64_decode($dateRange);
				if(false === $tmp) {// not valid base64 format
					$params['date_range'] = $dateRange;
				}
				else {// valid base64 format
					$tmp = json_decode($tmp, true);
					if(is_array($tmp)) {
						$params = $tmp;
					}
				}
			}

			// assign it to post for set_value()
			if(!empty($params['form'])) {
				$_POST['form'] = array();
				foreach($params['form'] as $k => $v) {
					$_POST['form'][$k] = $v;
				}
			}
		}


		// set default date range
		if(empty($params['date_range'])) {
			$params['date_range'] = 'today';
		}

		// state data
		$data['statedata'] = base64_encode(json_encode($params));


		// load data
		$pageOffset = isset($pageOffset)? (int) $pageOffset : 0;
		$perpage = 20;
		$data['logs_total'] = $this->actionlog_model->getListTotal($params);
		$data['logs'] = $this->actionlog_model->getList($params, $perpage, $pageOffset);

		// pagination
		$data['logs_pagelinks'] = WHelper::getPaginationHtml(site_url('actionlogs/form/' . $data['statedata'] . '/'), $data['logs_total'], $perpage, 4);

		// export link
		$data['logs_export_link'] = site_url('actionlogs/export/' . $data['statedata'] . '/');

		// form data
		$ddlAttr = array(
			'class'	=> 'select2 form-control',
			'data-minimum-results-for-search'	=> '10',
		);

		$formdata = array(
			'dateoption'	=> array(
				'today' => 'Today',
				'yesterday' => 'Yesterday',
//				'3dago' => '3 Days Ago',
				'1wago' => '1 Week Ago',
				'all' => 'All Time',
				'custom' => 'Custom',
			),
		);

		$data['formhtml'] = array(
			'dateoption'	=> form_dropdown('form[dateoption]', $formdata['dateoption'], set_value('form[dateoption]', ''), $ddlAttr),
			'date1'	=> form_input('form[date1]', set_value('form[date1]', ''), array('class' => 'form-control date-picker', 'placeholder' => 'eg: 2012-12-21')),
			'date2'	=> form_input('form[date2]', set_value('form[date2]', ''), array('class' => 'form-control date-picker', 'placeholder' => 'eg: 2012-12-21')),
		);

		$this->load->view('actionlogs/exportform', $data);
	}

	public function export($params=array()) {
		set_time_limit(0); // no timeout

		$this->output->set_template('blank');
		$this->load->helper('file');
		$this->load->helper('csv');

		if(!empty($params['date_range'])) {
			$dateRange = $params['date_range'];
		}
		else {
			if ($this->uri->segment(3)) {
				// possible values:
				// actionlogs/export/today/
				// actionlogs/export/eyJkYXRlX3JhbmdlIjoidG9kYXkifQ==/
				$dateRange = $this->uri->segment(3);

				if('' != $dateRange) {
					$tmp = base64_decode($dateRange);
					if(false === $tmp) {// not valid base64 format
						$params['date_range'] = $dateRange;
					}
					else {// valid base64 format
						$tmp = json_decode($tmp, true);
						if(is_array($tmp)) {
							$params = $tmp;
						}
					}

					if(!empty($params['date_range'])) {
						$dateRange = $params['date_range'];
					}
				}
			}
		}

		// add action log
		ActionLog::getInstance()->logLogsExport();


		// temp directory
		$tmpPath = FCPATH . 'tmp';
		if(!is_dir($tmpPath)) {
			mkdir($tmpPath, 0755);
		}

		// clean old files with 1/30 chance
		if(WHelper::hitOrMiss(30)) {
			WHelper::cleanDirectory($tmpPath, 1);
		}

		// file name & path
		if(!(isset($dateRange) && '' != $dateRange)) {
			$dateRange = 'alltime';
		}

		$fileName = $this->security->sanitize_filename("LogData-{$dateRange}-" . date('YmdHis'));
		$filePath = $tmpPath . '/' . $fileName . mt_rand(1, 999999) . '.csv';


		// row header
		$header = array(
			'Date',
			'User Name',
			'Action',
			'IP',
		);

		write_file($filePath, array_to_csv(array($header)), 'a');


		// total rows
		$total = $this->actionlog_model->getCsvDataTotal($params);
		if($total > 0) {
			$offset = -1;
			$perrun = 500;
			$maxrun = ceil($total / $perrun);

			for($i=0; $i<$maxrun; $i++) {
				$store = array();
				$offset = ($offset === -1)? 0 : $offset + $perrun;
				$csvDatas = $this->actionlog_model->getCsvData($params, $perrun, $offset);

				foreach($csvDatas as $csv) {
					$this->actionlog_model->processEntryData($csv, false);

					$uname = $csv->log_user_name;
					if('' != $csv->u_name && $uname !== $csv->u_name) {
						$uname .= " ($csv->u_name)";
					}

					$store[] = array(
						$csv->log_created,
						$uname,
						$csv->action_msg,
						$csv->log_ip,
					);
				}

				// write rows
				write_file($filePath, array_to_csv($store), 'a');
			}
		}


		// output
		header('Content-Type: application/csv');
		header('Content-Disposition: attachement; filename="' . $fileName . '.csv"');
		echo file_get_contents($filePath);
		exit;
	}

}
