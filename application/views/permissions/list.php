<?php
$colCnt = $isAdmin? 6 : 5;
?>
<style type="text/css">
	.card__header > h2 {
		margin-bottom: 8px;
	}
</style>

<script type="text/javascript">
	function deleteConfirm(url) {
		if (confirm('Do you want to delete this permanently?')) {
			window.location = url;
		}

		return false;
	}
</script>

<section id="content">
    <div class="card">
        <div class="card__header">
            <h2>Permission Table <small>Below are the list of permissions</small></h2>
        </div>


        <div class="card__body">
        	<?php if ($this->session->flashdata('success')) { ?>
				<div class="alert alert-success">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
				</div>

			<?php } else if ($this->session->flashdata('error')) { ?>
				<div class="alert alert-danger">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
				</div>

			<?php } else if ($this->session->flashdata('warning')) { ?>
				<div class="alert alert-warning">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>
				</div>

			<?php } else if ($this->session->flashdata('info')) { ?>
				<div class="alert alert-info">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
				</div>

			<?php } ?>
			<form id="permissions" action="<?php echo site_url('permissions/update'); ?>" method="post" >
            <div class="table-responsive">
                <table class="table">
                    <thead>
						<tr>
							<th>Role</th>
							<th>Create Super User</th>
							<th>Delete Super User</th>
							<th>Create Operator</th>
							<th>Delete Operator</th>
							<th>Group Assigning</th>
							<th>View Logs Report</th>
							<th>View Group List</th>
							<th>Account able to be deleted</th>
						</tr>
                    </thead>

                    <tbody>
						<?php if (empty($permissions['records'])): ?>
							<tr>
								<td colspan="3"><h3 style="color:red;">No permission Found!</h3></td>
							</tr>

						<?php else: ?>
							<?php foreach ($permissions['records'] as $key=>$val): $id = $val['id']; $permission = json_decode($val['permissions'],true); ?>
								<tr>
									<td>
										<?php
											if($val['role_id']=='2'){
												echo 'Admin';
											}
											else if($val['role_id']=='1'){
												echo 'Super User';
											}
											else if($val['role_id']=='0'){
												echo 'Operator';
											}
										?>
									</td>
									<td>
										<input type="checkbox" class="chkcol view" name="permissions[<?php echo $val['id']; ?>][admin][create]" value="true" <?php echo (isset($permission['admin']['create']) && $permission['admin']['create']=='true') || set_value('permissions['.$val['id'].'][admin][create]') ?'checked="checked"':''; ?> />
									</td>
									<td>
										<input type="checkbox" class="chkcol view" name="permissions[<?php echo $val['id']; ?>][admin][delete]" value="true" <?php echo (isset($permission['admin']['delete']) && $permission['admin']['delete']=='true') || set_value('permissions['.$val['id'].'][admin][delete]') ?'checked="checked"':''; ?> />
									</td>
									<td>
										<input type="checkbox" class="chkcol view" name="permissions[<?php echo  $val['id']; ?>][operator][create]" value="true" <?php echo (isset($permission['operator']['create']) && $permission['operator']['create']=='true') || set_value('permissions['.$val['id'].'][operator][create]') ?'checked="checked"':''; ?> />
									</td>
									<td>
										<input type="checkbox" class="chkcol view" name="permissions[<?php echo  $val['id']; ?>][operator][delete]" value="true" <?php echo (isset($permission['operator']['delete']) && $permission['operator']['delete']=='true') || set_value('permissions['.$val['id'].'][operator][delete]') ?'checked="checked"':''; ?> />
									</td>
									<td>
										<input type="checkbox" class="chkcol view" name="permissions[<?php echo  $val['id']; ?>][group][assigning]" value="true" <?php echo (isset($permission['group']['assigning']) && $permission['group']['assigning']=='true') || set_value('permissions['.$val['id'].'][group][assigning]') ?'checked="checked"':''; ?> />
									</td>
									<td>
										<input type="checkbox" class="chkcol view" name="permissions[<?php echo  $val['id']; ?>][log_report][view]" value="true" <?php echo (isset($permission['log_report']['view']) && $permission['log_report']['view']=='true') || set_value('permissions['.$val['id'].'][log_report][view]') ?'checked="checked"':''; ?> />
									</td>
									<td>
										<input type="checkbox" class="chkcol view" name="permissions[<?php echo  $val['id']; ?>][group][view]" value="true" <?php echo (isset($permission['group']['view']) && $permission['group']['view']=='true') || set_value('permissions['.$val['id'].'][group][view]') ?'checked="checked"':''; ?> />
									</td>
									<td>
										<input type="checkbox" class="chkcol view" name="permissions[<?php echo  $val['id']; ?>][account][delete]" value="true" <?php echo (isset($permission['account']['delete']) && $permission['account']['delete']=='true') || set_value('permissions['.$val['id'].'][account][delete]') ?'checked="checked"':''; ?> />
									</td>
								</tr>
							<?php endforeach; ?>
						<?php endif; ?>
                    </tbody>
                    <tfoot>
                    	<tr>
                    		<td colspan="8"><button class="btn btn-default">Update</button></td>
                    	</tr>
                    </tfoot>
                </table>
            </div>
            </form>
        </div>
    </div>
</section>
