<section id="content">

    <div class="card">

        <div class="card__header">

            <h2>Update Permission</h2>

        </div>



        <div class="card__body">

			<form id="group_add" action="<?php echo site_url('permissions/update/' . $permission['record']['id']); ?>" method="post" >

				<div class="row">

					<?php if ($this->session->flashdata('success')) { ?>

						<div class="alert alert-success">

							<a href="#" class="close" data-dismiss="alert">&times;</a>

							<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>

						</div>

					<?php } else if ($this->session->flashdata('error')) { ?>

						<div class="alert alert-danger">

							<a href="#" class="close" data-dismiss="alert">&times;</a>

							<strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>

						</div>

					<?php } else if ($this->session->flashdata('warning')) { ?>

						<div class="alert alert-warning">

							<a href="#" class="close" data-dismiss="alert">&times;</a>

							<strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>

						</div>

					<?php } else if ($this->session->flashdata('info')) { ?>

						<div class="alert alert-info">

							<a href="#" class="close" data-dismiss="alert">&times;</a>

							<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>

						</div>

					<?php } ?>





					<?php if (validation_errors()): ?>

						<div class="col-md-12">

							<div id="validation_errors" title="Error:">

								<?php echo validation_errors(); ?>

							</div>

						</div>

					<?php endif; ?>





					<div class="input-group">

                        <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>



                        <div class="form-group">

                            <input type="text" name="name" id="name" class="form-control" disabled="true" value="<?php echo $permission['record']['name'] ?>" />

							<i class="form-group__bar"></i>

                        </div>

                    </div>

<div class="row">
	<div class="col-xl-12">
		<table class="table table-hover table-striped">
			<thead>
				<tr>
					<th>Module</th>
					<th>View</th>
					<th>Create</th>
					<th>Edit</th>
					<th>Delete</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><label><input type="checkbox" class="chkrow" id="admin" value="true" /> Admin</label></td>
					<td><input type="checkbox" class="chkcol view" name="permissions[admin][view]" value="true" <?php echo (isset($permission['record']['permissions']['admin']['view']) && $permission['record']['permissions']['admin']['view']===true) || set_value('permissions[admin][view]') ?'checked="checked"':''; ?> /></td>
					<td><input type="checkbox" class="chkcol" name="permissions[admin][create]" value="true" <?php echo (isset($permission['record']['permissions']['admin']['create']) && $permission['record']['permissions']['admin']['create']===true)  || set_value('permissions[admin][create]')?'checked="checked"':''; ?> /></td>
					<td><input type="checkbox" class="chkcol" name="permissions[admin][edit]" value="true" <?php echo (isset($permission['record']['permissions']['admin']['edit']) && $permission['record']['permissions']['admin']['edit']===true)  || set_value('permissions[admin][edit]')?'checked="checked"':''; ?> /></td>
					<td><input type="checkbox" class="chkcol" name="permissions[admin][delete]" value="true" <?php echo (isset($permission['record']['permissions']['admin']['delete']) && $permission['record']['permissions']['admin']['delete']===true)  || set_value('permissions[admin][delete]')?'checked="checked"':''; ?> /></td>
				</tr>
				<tr>
					<td><label><input type="checkbox" class="chkrow" id="operator" value="true" /> Operator</label></td>
					<td><input type="checkbox" class="chkcol view" name="permissions[operator][view]" value="true" <?php echo (isset($permission['record']['permissions']['operator']['view']) && $permission['record']['permissions']['operator']['view']===true) || set_value('permissions[operator][view]') ?'checked="checked"':''; ?> /></td>
					<td><input type="checkbox" class="chkcol" name="permissions[operator][create]" value="true" <?php echo (isset($permission['record']['permissions']['operator']['create']) && $permission['record']['permissions']['operator']['create']===true)  || set_value('permissions[operator][create]')?'checked="checked"':''; ?> /></td>
					<td><input type="checkbox" class="chkcol" name="permissions[operator][edit]" value="true" <?php echo (isset($permission['record']['permissions']['operator']['edit']) && $permission['record']['permissions']['operator']['edit']===true)  || set_value('permissions[operator][edit]')?'checked="checked"':''; ?> /></td>
					<td><input type="checkbox" class="chkcol" name="permissions[operator][delete]" value="true" <?php echo (isset($permission['record']['permissions']['operator']['delete']) && $permission['record']['permissions']['operator']['delete']===true)  || set_value('permissions[operator][delete]')?'checked="checked"':''; ?> /></td>
				</tr>
				<tr>
					<td><label><input type="checkbox" class="chkrow" id="permission" value="true" /> Permission</label></td>
					<td><input type="checkbox" class="chkcol view" name="permissions[permission][view]" value="true" <?php echo (isset($permission['record']['permissions']['permission']['view']) && $permission['record']['permissions']['permission']['view']===true) || set_value('permissions[permission][view]') ?'checked="checked"':''; ?> /></td>
					<td><input type="checkbox" class="chkcol" name="permissions[permission][create]" value="true" <?php echo (isset($permission['record']['permissions']['permission']['create']) && $permission['record']['permissions']['permission']['create']===true)  || set_value('permissions[permission][create]')?'checked="checked"':''; ?> /></td>
					<td><input type="checkbox" class="chkcol" name="permissions[permission][edit]" value="true" <?php echo (isset($permission['record']['permissions']['permission']['edit']) && $permission['record']['permissions']['permission']['edit']===true)  || set_value('permissions[permission][edit]')?'checked="checked"':''; ?> /></td>
					<td><input type="checkbox" class="chkcol" name="permissions[permission][delete]" value="true" <?php echo (isset($permission['record']['permissions']['permission']['delete']) && $permission['record']['permissions']['permission']['delete']===true)  || set_value('permissions[permission][delete]')?'checked="checked"':''; ?> /></td>
				</tr>
				<tr>
					<td><label><input type="checkbox" class="chkrow" id="group" value="true" /> Group</label></td>
					<td><input type="checkbox" class="chkcol view" name="permissions[group][view]" value="true" <?php echo (isset($permission['record']['permissions']['group']['view']) && $permission['record']['permissions']['group']['view']===true) || set_value('permissions[group][view]') ?'checked="checked"':''; ?> /></td>
					<td><input type="checkbox" class="chkcol" name="permissions[group][create]" value="true" <?php echo (isset($permission['record']['permissions']['group']['create']) && $permission['record']['permissions']['group']['create']===true)  || set_value('permissions[group][create]')?'checked="checked"':''; ?> /></td>
					<td><input type="checkbox" class="chkcol" name="permissions[group][edit]" value="true" <?php echo (isset($permission['record']['permissions']['group']['edit']) && $permission['record']['permissions']['group']['edit']===true)  || set_value('permissions[group][edit]')?'checked="checked"':''; ?> /></td>
					<td><input type="checkbox" class="chkcol" name="permissions[group][delete]" value="true" <?php echo (isset($permission['record']['permissions']['group']['delete']) && $permission['record']['permissions']['group']['delete']===true)  || set_value('permissions[group][delete]')?'checked="checked"':''; ?> /></td>
				</tr>
				<tr>
					<td><label><input type="checkbox" class="chkrow" id="group_report" value="true" /> Group Report</label></td>
					<td><input type="checkbox" class="chkcol view" name="permissions[group_report][view]" value="true" <?php echo (isset($permission['record']['permissions']['group_report']['view']) && $permission['record']['permissions']['group_report']['view']===true) || set_value('permissions[group_report][view]') ?'checked="checked"':''; ?> /></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td><label><input type="checkbox" class="chkrow" id="log_report" value="true" /> Log Report</label></td>
					<td><input type="checkbox" class="chkcol view" name="permissions[log_report][view]" value="true" <?php echo (isset($permission['record']['permissions']['log_report']['view']) && $permission['record']['permissions']['log_report']['view']===true) || set_value('permissions[log_report][view]') ?'checked="checked"':''; ?> /></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td><label><input type="checkbox" class="chkrow" id="account" value="true" /> Account</label></td>
					<td></td>
					<td></td>
					<td></td>
					<td><input type="checkbox" class="chkcol" name="permissions[account][delete]" value="true" <?php echo (isset($permission['record']['permissions']['account']['delete']) && $permission['record']['permissions']['account']['delete']===true)  || set_value('permissions[account][delete]')?'checked="checked"':''; ?> /></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

                    <br/>

                    <button class="btn btn-default">Update</button>

				</div>

			</form>

        </div>

    </div>

</section>

<script type="text/javascript">
	$(document).ready(function(){

	$(this).closest('tr').find('.chkcol:checked');
		$(this).find('.view').each(function () {
           if (this.checked) {
               $(this).closest('tr').find('.chkcol').removeAttr("disabled");
           }else{
           	$(this).closest('tr').find('.chkcol').attr("disabled", true).attr("checked", false);
           	$(this).removeAttr("disabled");
           }
		});
		// $(".chkcol").attr("disabled", true);
		// $(".view").removeAttr("disabled");

		$('.view').click(function(){
			if (this.checked) {
			 $(this).closest('tr').find('.chkcol').removeAttr("disabled");

			}else{
			 $(this).closest('tr').find('.chkcol').attr("disabled", true).attr("checked", false);
			 $(this).closest('tr').find('.chkcol').prop("checked", false);
			 $(".view").removeAttr("disabled");
			}
		});

		$('.chkrow').click(function(){
			$(this).closest('tr').find('input:checkbox').not(this).prop('checked', this.checked);
			if(this.checked){
			$(this).closest('tr').find('.chkcol').removeAttr("disabled");
			}else{
				$(this).closest('tr').find('.chkcol').attr("disabled", true).attr("checked", false);
				$(".view").removeAttr("disabled");
			}

		});
		$('.chkcol').click(function(){
			if ($(this).closest('tr').find('.chkcol:checked').length == $(this).closest('tr').find('.chkcol').length) {
					$(this).closest('tr').find('.chkrow').prop('checked', true);
					$(this).closest('tr').find('.chkcol').removeAttr("disabled");
			}
			else{
				// $(this).closest('tr').find('.chkrow').prop('checked', false);
			}
		})
	
});
</script>