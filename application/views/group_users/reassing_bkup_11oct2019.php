<?php
$colCntFree = 11; // 12
$colCntAssigned = 6;
?>
<style>
	div.pager {
		text-align: center;
		margin: 1em 0;
	}

	div.pager span {
		display: inline-block;
		padding: 5px;
		line-height: 1.8;
		text-align: center;
		cursor: pointer;
		background: #000;
		color: #fff;
		margin-right: 0.5em;
	}

	div.pager span.active {
		background: #c00;
	}

	.group-in,
	.group-diff {
		color: #edeff0;
		opacity: 1 !important;
	}

	.group-in {
		background: #4caf50;
	}

	.group-diff {
		background: #ef5350;
	}

	.checkall-toggle {
		margin: 0;
	}
</style>


<section id="content">
	<div class="card">
		<div class="card__header">
			<h2>Users Group Relation<small></small></h2>
		</div>


		<div class="card__body">
			<?php if ($this->session->flashdata('success')): ?>
				<div class="alert alert-success">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
				</div>
			<?php endif; ?>

			<?php if ($this->session->flashdata('error')): ?>
				<div class="alert alert-danger">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
				</div>
			<?php endif; ?>

			<?php if ($this->session->flashdata('warning')): ?>
				<div class="alert alert-warning">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>
				</div>
			<?php endif; ?>

			<?php if ($this->session->flashdata('info')): ?>
				<div class="alert alert-info">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
				</div>
			<?php endif; ?>


			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Created By</th>
							<th>Date</th>
							<th>Time</th>
							<th>Escort</th>
							<th>Visitor</th>
							<th>Action</th>
						</tr>
					</thead>

					<tbody>
						<tr>
							<td><?php echo $group['0']->id; ?></td>
							<td><?php echo $group['0']->group_name; ?></td>
							<td><?php echo $group['0']->name; ?></td>
							<td><?php echo date('m/d/Y', strtotime($group['0']->created_time)); ?></td>
							<td><?php echo date('H:i s', strtotime($group['0']->created_time)); ?></td>
							<td>
								<span data-type="group-tap-count" data-taptype="escort" data-tapid="<?php echo $group['0']->id; ?>"><?php echo $group['0']->escort_in_count; ?></span>
							</td>
							<td>
								<span data-type="group-tap-count" data-taptype="visitor" data-tapid="<?php echo $group['0']->id; ?>"><?php echo $group['0']->visitor_in_count; ?></span>
							</td>
							<td>
								<a class="btn btn-default" href="<?php echo site_url('groups/resetUsers/' . $group['0']->id); ?>">Reset</a>
								<input type="hidden" id="id" name="id" value="<?php echo $group['0']->id; ?>" />
							</td>
						</tr>
					</tbody>
				</table>
			</div>


			<div class="table-responsive userlist userlist-assigned">
				<h3 class="text-center">Users List (Assigned)</h3>

				<form id="group_add" action="<?php echo site_url('group-users/dissociate/' . $group['0']->id); ?>" method="post">
					<table class="table">
						<thead>
							<tr>
								<th>
									<div class="checkbox checkall-toggle">
										<label>
											<input type="checkbox" value="" title="Check All Items" onclick="WScript.checkAll(this, 'cb-dissociate')" />
											<i class="input-helper"></i>
										</label>
									</div>
								</th>
								<th>Name</th>
								<th>Card Number</th>
								<th>Family Number</th>
								<th>Status</th>
								<th>Type</th>
							</tr>
						</thead>

						<tbody>
							<?php if (empty($group_users)): ?>
								<tr>
									<td colspan="<?php echo $colCntAssigned; ?>"><h3 style="color:red;">No user found in group</h3></td>
								</tr>

							<?php else: ?>
								<?php
								$i = 0;
								foreach ($group_users as $guser):
									$i++;
//									$attr = array();
									$val = $guser->user_id;
									$tapStatus = ($guser->has_tap)? 'IN' : 'OUT';
									$utype = ($guser->cf_visitor == 0)? 'Escort' : 'Visitor';
								?>
									<tr>
										<td>
											<div class="checkbox">
												<label>
													<input type="checkbox" id="cb-dissociate<?php echo $i; ?>" name="dissociate[]" value="<?php echo $val; ?>" <?php // echo $attr? implode(' ', $attr) : ''; ?> />
													<i class="input-helper"></i>
													<?php echo $val; ?>
												</label>
											</div>
										</td>
										<td><?php echo $guser->name; ?></td>
										<td><?php echo (empty($guser->card_number))? 0 : $guser->card_number; ?></td>
										<td><?php echo (empty($guser->family_number))? 0 : $guser->family_number; ?></td>
										<td>
											<span data-type="group-user-tap-status" data-userid="<?php echo $val; ?>"><?php echo $tapStatus; ?></span>
										</td>
										<td><?php echo $utype; ?></td>
									</tr>
								<?php endforeach; ?>
								<?php
									if(chkaccess($userrole,'group','assigning',true)!='0'){
										?>
								<tr>
									<td colspan="<?php echo $colCntAssigned; ?>">
										<button type="submit" name="submit" class="btn btn-default">Unassign User</button>
										<input type="hidden" name="formdata" value="<?php echo $formdata; ?>" />
									</td>
								</tr>
								<?php } ?>
							<?php endif; ?>
						</tbody>
					</table>
				</form>
			</div>




			<h3 class="text-center">Users List (All)</h3>

			<form id="group_add" action="<?php echo site_url('group-users/reassing/' . $group['0']->id); ?>" method="post">
				<div class="row">
					<div class="col-sm-2">
						<h4>Search Users :</h4>
					</div>

					<div class="col-sm-10">
						<div class="row">
							<div class="col-sm-10">
								<div class="form-group">
									<?php echo $formhtml['search']; ?>
								</div>
							</div>

							<div class="col-sm-2">
								<input type="submit" name="search" value="Search" class="btn btn-default" />
							</div>
						</div>
					</div>
				</div>


				<div class="row">
					<div class="col-sm-2">
						<p>Search Scope :</p>
					</div>

					<div class="col-sm-10">
						<div class="row">
							<div class="col-sm-10">
								<div class="form-group">
									<?php echo $formhtml['search_scope']; ?>
								</div>
							</div>
						</div>
					</div>
				</div>


				<?php if (true || !empty($all_users)): ?>
					<div class="row">
						<div class="col-sm-3">
							<p>Designation</p>

							<div class="form-group">
								<?php echo $formhtml['designation']; ?>
							</div>
						</div>

						<div class="col-sm-3">
							<p>Department</p>

							<div class="form-group">
								<?php echo $formhtml['department']; ?>
							</div>
						</div>

						<div class="col-sm-3">
							<p>Division</p>

							<div class="form-group">
								<?php echo $formhtml['division']; ?>
							</div>
						</div>

						<div class="col-sm-3">
							<p>Team</p>

							<div class="form-group">
								<?php echo $formhtml['team']; ?>
							</div>
						</div>

						<div class="col-sm-3">
							<p>Company</p>

							<div class="form-group">
								<?php echo $formhtml['company']; ?>
							</div>
						</div>

						<div class="col-sm-3">
							<p>Type</p>

							<div class="form-group">
								<?php echo $formhtml['visitor']; ?>
							</div>
						</div>
					</div>
				<?php endif; ?>

				<input type="hidden" name="nodelete" value="No" />
			</form>




			<form id="group_add" action="<?php echo site_url('group-users/assign/' . $group_id); ?>" method="post">
				<?php if (false): ?>
				<?php
				$page = 1;
				if ($this->uri->segment('4')) {
					$page = $this->uri->segment('4');
				}

				$startpage = 1;
				$endpage = 10;

				if ($page > 6) {
					$startpage = $page - 4;
					$endpage = $page + 5;
				}

				if (!$all_users) {
					$startpage = $page - 10;
					$endpage = $page;
				}
				?>
				<div class="pager">
					<?php for ($k = $startpage; $k <= $endpage; $k++) { ?>
						<span class="page-number <?php if ($page == $k) {
							echo 'active';
						} else {
							echo 'clickable';
						} ?>">
							<a href="<?php echo site_url('group-users/reassing/' . $group['0']->id . '/' . $k); ?>"><?php echo $k; ?></a>
						</span>
					<?php } ?>
				</div>
				<?php endif; ?>


				<div class="table-responsive">
					<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>#</th>
								<th>Group</th>
								<th>Name</th>
								<th>NRIC</th>
								<th>Department</th>
								<th>Division</th>
								<th>Team</th>
								<th>Company</th>
								<!--<th>Disabled</th>-->
								<th>Card Number</th>
								<th>Family Number</th>
								<th>Type</th>
							</tr>
						</thead>

						<tbody>
							<?php if (empty($all_users)): ?>
								<tr>
									<td colspan="<?php echo $colCntFree; ?>"><h3 style="color:red;">No user Found!</h3></td>
								</tr>

								<tr>
									<td colspan="<?php echo $colCntFree; ?>">
										<a href="<?php echo site_url('gx-users/refresh/'); ?>"
											class="btn btn-default"
											title="Flush old data then retrieve new set of users data"
											target="_blank">Refresh Users</a>
									</td>
								</tr>

							<?php else: ?>
								<?php
//								$CurrentPageUser = array();
								foreach ($all_users as $guser):
									$attr = array();
									$classCbDiv = array();
//									$val = $guser->user_id . '##' . $guser->cf_visitor . '##' . $guser->card_number . '##' . $guser->family_number;
									$val = $guser->user_id;

									if ($guser->group_id > 0) {
										if ($guser->group_id == $group_id) {
											$attr[] = 'disabled="disabled"';
											$classCbDiv[] = 'disabled group-in';
										}
										else {
											$attr[] = 'disabled="disabled"';
											$classCbDiv[] = 'disabled group-diff';
										}
									}

									$utype = ($guser->cf_visitor == 0)? 'Escort' : 'Visitor';
								?>
									<tr>
										<td>
											<div class="checkbox <?php echo implode(' ', $classCbDiv); ?>">
												<label>
													<input type="checkbox" name="assign[]" value="<?php echo $val; ?>" <?php echo $attr? implode(' ', $attr) : ''; ?> />
													<i class="input-helper"></i>
													<?php echo $guser->user_id; ?>
												</label>
											</div>
										</td>
										<td><?php echo $guser->group_name; ?></td>
										<td><?php echo $guser->name; ?></td>
										<td><?php echo $guser->cf_nric; ?></td>
										<td><?php echo $guser->cf_department; ?></td>
										<td><?php echo $guser->cf_division; ?></td>
										<td><?php echo $guser->cf_team; ?></td>
										<td><?php echo $guser->cf_company; ?></td>

										<!--<td><?php // echo $guser->user_disabled? 'true' : 'false'; ?></td>-->

										<td><?php echo (empty($guser->card_number))? 0 : $guser->card_number; ?></td>
										<td><?php echo (empty($guser->family_number))? 0 : $guser->family_number; ?></td>
										<td><?php echo $utype; ?></td>
									</tr>
								<?php endforeach; ?>


								<tr>
									<td colspan="<?php echo $colCntFree; ?>">
										<?php
									if(chkaccess($userrole,'group','assigning',true)!='0'){
										?>
										<button type="submit" name="submit" class="btn btn-default">Assign User</button>
										<?php } ?>
										<a href="<?php echo site_url('gx-users/refresh/'); ?>"
											class="btn btn-default"
											title="Flush old data then retrieve new set of users data"
											>Refresh Users</a>

										<input type="hidden" name="formdata" value="<?php echo $formdata; ?>" />
									</td>
								</tr>
							<?php endif; ?>
						</tbody>
					</table>
				</div>
			</form>
		</div>
	</div>
</section>


<?php if (!empty($group)): ?>
<script type="text/javascript">
	var WScript = WScript || {};
	WScript.checkAll = function(checkbox, stub) {
        if (!checkbox.form) {
			return false;
		}

        stub = stub ? stub : "cb";
        var c = 0, i, e, n;
        for (i = 0,
        n = checkbox.form.elements.length; i < n; i++) {
            e = checkbox.form.elements[i];
            if (e.type == checkbox.type && e.id.indexOf(stub) === 0) {
                e.checked = checkbox.checked;
                c += e.checked ? 1 : 0;
            }
        }

        if (checkbox.form.boxchecked) {
            checkbox.form.boxchecked.value = c;
        }

        return true;
    }


	!function($){
		$(document).ready(function(){
			var $wrapper = $('#content');

			setTimeout(refreshGroupsTapCount, getRefreshTime());

			function refreshGroupsTapCount() {
				var $ids = $wrapper.find('[data-type="group-tap-count"][data-tapid]'), ids = [];

				$ids.each(function(){
					ids.push($(this).data('tapid'));
				});

				$.getJSON(
					<?php echo json_encode($groupInfoUrl); ?>,
					{id: ids},
					function(resp){
						if('undefined' === typeof resp.items || !resp.items.length) {
							return;
						}

						for(var i in resp.items) {
							var item = resp.items[i];

							$wrapper.find('[data-tapid="'+item.id+'"]').each(function(){
								var $this = $(this);
								var num = ('escort' === $this.data('taptype'))?
									item.escort_in_count : item.visitor_in_count;

								$this.hide().html(num).fadeIn();
							});

							// group users
							if(typeof item.users !== 'undefined' && item.users.length > 0) {
								for(var i=0; i<item.users.length; i++) {
									var user = item.users[i];
									$wrapper.find('[data-type="group-user-tap-status"][data-userid="'+user.userId+'"]').html(user.tapStatus).fadeIn();
								}
							}
						}

						setTimeout(refreshGroupsTapCount, getRefreshTime());
					}
				);
			}

			function getRefreshTime() {
				var min = 5, max = 10;
				return (Math.random() * (max - min) + min) * 1000;
			}
		});
	}(jQuery);
</script>
<?php endif; ?>
