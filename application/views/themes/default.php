<?php

$siteName = $this->config->item('site_name');

$siteSubheader = $this->config->item('site_subheader');

$headbandColor = $this->config->item('headband_color');

$headbandTxtColor = $this->config->item('headband_text_color');



if($siteName) {

//	$this->output->set_title($siteName);

	$title = empty($title)? $siteName : "{$siteName} - {$title}";

}

?>

<!DOCTYPE html>

<html lang="en">

	<!--[if IE 9 ]><html class="ie9"><![endif]-->

	<head>

		<meta charset="utf-8" />

		<meta http-equiv="X-UA-Compatible" content="IE=edge" />

		<meta name="viewport" content="width=device-width, initial-scale=1" />

		<title><?php echo $title; ?></title>



		<!-- Animate CSS -->

		<link href="<?php echo base_url(); ?>assets/themes/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet" />

		<!-- Material Design Icons -->

		<link href="<?php echo base_url(); ?>assets/themes/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet" />

		<!-- Malihu Scrollbar -->

		<link href="<?php echo base_url(); ?>assets/themes/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet" />

		<!-- SweetAlert -->

		<link href="<?php echo base_url(); ?>assets/themes/vendors/bower_components/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet" />

		<!-- Full Calendar -->

		<link href="<?php echo base_url(); ?>assets/themes/vendors/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet" />

		<!-- Demo only -->

		<link href="<?php echo base_url(); ?>assets/themes/demo/css/demo.css" rel="stylesheet" />

		<link href="<?php echo base_url(); ?>assets/themes/css/dataTables.bootstrap.min.css" rel="stylesheet" />



		<?php /* all css in themes/vendors/bower_components must loaded before app-1.min.css */ ?>

		<link href="<?php echo base_url(); ?>assets/themes/vendors/bower_components/select2/dist/css/select2.css" rel="stylesheet" />

		<link href="<?php echo base_url(); ?>assets/themes/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />



		<!-- Site CSS -->

		<link href="<?php echo base_url(); ?>assets/themes/css/app-1.min.css" rel="stylesheet" />



		<!-- Page loader -->

		<script src="<?php echo base_url(); ?>assets/themes/js/page-loader.min.js"></script>

		<!-- jQuery -->

		<script src="<?php echo base_url(); ?>assets/themes/vendors/bower_components/jquery/dist/jquery.min.js"></script>

		<!-- Bootstrap -->

		<script src="<?php echo base_url(); ?>assets/themes/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

		<script src="<?php echo base_url(); ?>assets/themes/js/jquery.dataTables.min.js"></script>

		<script src="<?php echo base_url(); ?>assets/themes/js/dataTables.bootstrap.min.js"></script>



		<style>

			body:before {

				height: auto !important;

				padding: 2px 0;

				text-align: center;

				font-weight: bold;

				font-size: 18px;

				color: <?php echo $headbandTxtColor? $headbandTxtColor : '#fff'; ?>;

				background-color: <?php echo $headbandColor? $headbandColor : '#000'; ?>;

				z-index: 1;



				content: "<?php echo htmlspecialchars($siteName? $siteName : $siteSubheader); ?>";

			}

		</style>

	</head>





	<body>

		<!-- Page Loader -->

		<div id="page-loader">

			<div class="preloader preloader--xl preloader--light">

				<svg viewBox="25 25 50 50">

				<circle cx="50" cy="50" r="20" />

				</svg>

			</div>

		</div>



		<!-- Header -->

		<header id="header">

			<div class="logo">

				<a href="<?php echo base_url(); ?>" class="hidden-xs">

					<?php echo $siteName; ?>



					<?php if (!empty($siteSubheader)): ?>

						<small><?php echo $siteSubheader; ?></small>

					<?php endif; ?>

				</a>

				<i class="logo__trigger zmdi zmdi-menu" data-mae-action="block-open" data-mae-target="#navigation"></i>

			</div>



			<ul class="top-menu">

				<li class="top-menu__alerts" data-mae-action="block-open" data-mae-target="#notifications" data-toggle="tab" data-target="#notifications__messages">

					<!--<a href=""><i class="zmdi zmdi-notifications"></i></a>-->

				</li>



				<li class="top-menu__profile dropdown">

					<a data-toggle="dropdown" href="">

						<img src="<?php echo base_url(); ?>assets/themes/img/logo.png" alt="" />

					</a>



					<ul class="dropdown-menu pull-right dropdown-menu--icon">

						<?php if (FALSE): ?>

						<li>

							<a href="<?php echo site_url('users/profile'); ?>"><i class="zmdi zmdi-account"></i> View Profile</a>

						</li>

						<?php endif; ?>



						<li>

							<a href="<?php echo site_url('users/password'); ?>"><i class="zmdi zmdi-account"></i> Change Password</a>

						</li>



						<li>

							<a href="<?php echo site_url('users/logout'); ?>"><i class="zmdi zmdi-time-restore"></i> Logout</a>

						</li>

					</ul>

				</li>

			</ul>



			<!--<form class="top-search">

				<input type="text" class="top-search__input" placeholder="Search for people, files & reports">

				<i class="zmdi zmdi-search top-search__reset"></i>

			</form>-->

		</header>





		<section id="main">

			<aside id="notifications">

				<ul class="tab-nav tab-nav--justified tab-nav--icon">

					<li><a class="user-alert__messages" href="#notifications__messages" data-toggle="tab"><i class="zmdi zmdi-email"></i></a></li>

					<li><a class="user-alert__notifications" href="#notifications__updates" data-toggle="tab"><i class="zmdi zmdi-notifications"></i></a></li>

					<li><a class="user-alert__tasks" href="#notifications__tasks" data-toggle="tab"><i class="zmdi zmdi-playlist-plus"></i></a></li>

				</ul>

			</aside>



			<aside id="navigation">

				<div class="navigation__header">

					<i class="zmdi zmdi-long-arrow-left" data-mae-action="block-close"></i>

				</div>



				<div class="navigation__menu c-overflow">

					<ul>

						<li>

							<a href="<?php echo site_url(); ?>"><i class="zmdi zmdi-home"></i> Home</a>

						</li>



						<li class="navigation__sub">

							<a href="<?php echo site_url('users'); ?>" data-mae-action="submenu-toggle"><i class="zmdi zmdi-accounts"></i>Staffs</a>



							<ul>
								<?php
							if($this->session->userdata('logged_in')['isSuper']!='0')
							{
								?>
								<li><a href="<?php echo site_url('users/add'); ?>">Add Staff</a></li>
								<?php } ?>

								<li><a href="<?php echo site_url('users/staffs'); ?>">Staffs List</a></li>

							</ul>

						</li>



						<li class="navigation__sub">

							<a href="<?php echo site_url('groups'); ?>" data-mae-action="submenu-toggle"><i class="zmdi zmdi-widgets"></i>Groups</a>



							<ul>

								<li><a href="<?php echo site_url('groups/add'); ?>">Add Group</a></li>

								<li><a href="<?php echo site_url('groups'); ?>">Groups List</a></li>

								<li><a href="<?php echo site_url('groups/export'); ?>">Groups Report</a></li>

							</ul>

						</li>



						<?php if (FALSE): ?>

						<li><a href="<?php echo site_url('group-users/getcsv'); ?>"><i class="zmdi zmdi-file"></i> Groups Report</a></li>



						<li class="navigation__sub">

							<a href="<?php echo site_url('actionlogs/export'); ?>" data-mae-action="submenu-toggle"><i class="zmdi zmdi-file-text"></i>Logs Report</a>



							<ul>

								<li><a href="<?php echo site_url('actionlogs/export/today'); ?>">Today</a></li>

								<li><a href="<?php echo site_url('actionlogs/export/yesterday'); ?>">Yesterday</a></li>

								<li><a href="<?php echo site_url('actionlogs/export/3dago'); ?>">3 Days Ago</a></li>

								<li><a href="<?php echo site_url('actionlogs/export/1wago'); ?>">1 Week Ago</a></li>

								<li><a href="<?php echo site_url('actionlogs/export'); ?>">All Time</a></li>

							</ul>

						</li>

						<?php endif; ?>



						<li><a href="<?php echo site_url('actionlogs/'); ?>"><i class="zmdi zmdi-file"></i> Logs Report</a></li>
						<?php
							if($this->session->userdata('logged_in')['isSuper']=='2'){
								?>
								<li><a href="<?php echo site_url('permissions/'); ?>"><i class="zmdi zmdi-lock-open"></i> Permissions</a></li>
								<?php
							}
						?>

						<li><a href="<?php echo site_url('users/logout'); ?>"><i class="zmdi zmdi-power"></i> Logout</a></li>

					</ul>

				</div>

			</aside>



			<section id="content" class="content--boxed">
				<?php if ($this->session->userdata('logged_in_success')) { ?>
					<div class="alert alert-success">
						<a href="#" class="close" data-dismiss="alert">&times;</a>
						<?php echo $this->session->userdata('logged_in_success'); ?>
					</div>
				<?php } ?>
				<?php echo $output; ?>

			</section>



			<footer id="footer">

				Copyright &copy; <?php echo date('Y'); ?>

			</footer>

		</section>



		<!-- Malihu ScrollBar -->

		<script src="<?php echo base_url(); ?>assets/themes/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>



		<!-- Bootstrap Notify -->

		<script src="<?php echo base_url(); ?>assets/themes/vendors/bower_components/remarkable-bootstrap-notify/dist/bootstrap-notify.min.js"></script>



		<!-- SweetAlert -->

		<script src="<?php echo base_url(); ?>assets/themes/vendors/bower_components/sweetalert2/dist/sweetalert2.min.js"></script>



		<!-- FullCalendar -->

		<script src="<?php echo base_url(); ?>assets/themes/vendors/bower_components/moment/min/moment.min.js"></script>

		<script src="<?php echo base_url(); ?>assets/themes/vendors/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>



		<script src="<?php echo base_url(); ?>assets/themes/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

		<script src="<?php echo base_url(); ?>assets/themes/vendors/bower_components/select2/dist/js/select2.min.js"></script>



		<!-- Placeholder for IE9 -->

		<!--[if IE 9 ]>

			<script src="<?php echo base_url(); ?>assets/themes/vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js"></script>

		<![endif]-->



		<!-- Demo Only -->

		<script src="<?php echo base_url(); ?>assets/themes/demo/js/misc.js"></script>

		<script src="<?php echo base_url(); ?>assets/themes/demo/js/calendar.js"></script>



		<!-- Site Functions & Actions -->

		<script src="<?php echo base_url(); ?>assets/themes/js/app.min.js"></script>

	</body>

</html>

