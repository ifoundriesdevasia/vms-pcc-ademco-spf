<?php
$colCnt = $isAdmin? 6 : 5;
?>
<style type="text/css">
	.card__header > h2 {
		margin-bottom: 8px;
	}
</style>

<script type="text/javascript">
	function deleteConfirm(url) {
		if (confirm('Do you want to delete this permanently?')) {
			window.location = url;
		}

		return false;
	}
</script>

<section id="content">
    <div class="card">
        <div class="card__header">
            <h2>Staff Table <small>Below are the list of Staff</small></h2>
            <?php
				if($this->session->userdata('logged_in')['isSuper']!='0')
				{
					?>
            <a class="btn btn-default" href="<?php echo site_url('users/add'); ?>">Add New</a>
            <?php } ?>
        </div>


        <div class="card__body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Email</th>
							<th>Level</th>
							<th>Phone</th>
							<th>Date</th>

							<?php // if ($isAdmin): ?>
								<th>Action</th>
							<?php // endif; ?>
						</tr>
                    </thead>

                    <tbody>
						<?php if (empty($userlist)): ?>
							<tr>
								<td colspan="<?php echo $colCnt; ?>"><h3 style="color:red;">No user Found!</h3></td>
							</tr>

						<?php else: ?>
							<?php 
							foreach ($userlist as $user): ?>

								<tr>
									<td><?php echo $user->id; ?></td>
									<td><?php echo $user->name; ?></td>
									<td><?php echo $user->email; ?></td>
									<td>
										<?php if($user->isSuper == '0') { echo '<label style="padding:5px;" class="btn--light">Operator</label>'; }
										else if($user->isSuper=='1') { echo '<label style="padding:5px;" class="btn--light">Super User</label>'; }
										else if($user->isSuper=='2') { echo '<label style="padding:5px;" class="btn--light">Admin</label>'; }
										?>
									</td>
									<td><?php echo $user->phone; ?></td>
									<td><?php echo date('d M, Y', strtotime($user->created)); ?></td>

									<?php // if ($isAdmin): ?>
										<td>
											<?php
												$r = '';
												if($user->isSuper=='0'){
													$r = 'operator';
												}
												else if($user->isSuper=='1'){
													$r = 'admin';
												}
												else{
												}
												if($this->session->userdata('logged_in')['id']==$user->id){
													?>
													<a href="<?php echo site_url('users/update/' . $user->id); ?>" class="btn btn--light btn-xs">Edit</a>&nbsp;&nbsp;
													<?php
												}
												else if($this->session->userdata('logged_in')['isSuper']=='0'){
													if($user->isSuper=='0' && chkaccess($userrole,'operator','create',true)!='0'){
														?>
														<a href="<?php echo site_url('users/update/' . $user->id); ?>" class="btn btn--light btn-xs">Edit</a>&nbsp;&nbsp;
														<?php
													}
													else if($user->isSuper=='1' && chkaccess($userrole,'admin','create',true)!='0'){
														?>
														<a href="<?php echo site_url('users/update/' . $user->id); ?>" class="btn btn--light btn-xs">Edit</a>&nbsp;&nbsp;
														<?php
													}
												}
												else if($this->session->userdata('logged_in')['isSuper']=='1'){
													if($user->isSuper=='0' && chkaccess($userrole,'operator','create',true)!='0'){
														?>
														<a href="<?php echo site_url('users/update/' . $user->id); ?>" class="btn btn--light btn-xs">Edit</a>&nbsp;&nbsp;
														<?php
													}
													else if($user->isSuper=='1' && chkaccess($userrole,'admin','create',true)!='0'){
														?>
														<a href="<?php echo site_url('users/update/' . $user->id); ?>" class="btn btn--light btn-xs">Edit</a>&nbsp;&nbsp;
														<?php
													}
												}
												else if($this->session->userdata('logged_in')['isSuper']=='2'){
													?>
													<a href="<?php echo site_url('users/update/' . $user->id); ?>" class="btn btn--light btn-xs">Edit</a>&nbsp;&nbsp;
													<?php
												}

												if(userchkaccess($alluserrole,'account','delete',$user->isSuper)!='1')
												{
												if($this->session->userdata('logged_in')['isSuper']=='2' && chkaccess($userrole,$r,'delete',true)!='0'){
													?>
													<a href="#delete"
												class="btn btn--light btn-xs"
												onclick="deleteConfirm('<?php echo site_url('users/delete/' . $user->id); ?>');">Delete</a>
													<?php
												}

												else if($this->session->userdata('logged_in')['isSuper']=='0'){
													if($user->isSuper=='0' && chkaccess($userrole,'operator','delete',true)!='0'){
														?>
														<a href="#delete"
												class="btn btn--light btn-xs"
												onclick="deleteConfirm('<?php echo site_url('users/delete/' . $user->id); ?>');">Delete</a>
														<?php
													}
													else if($user->isSuper=='1' && chkaccess($userrole,'admin','delete',true)!='0'){
														?>
														<a href="#delete"
												class="btn btn--light btn-xs"
												onclick="deleteConfirm('<?php echo site_url('users/delete/' . $user->id); ?>');">Delete</a>
														<?php
													}
												}
												else if($this->session->userdata('logged_in')['isSuper']=='1'){
													if($user->isSuper=='0' && chkaccess($userrole,'operator','delete',true)!='0'){
														?>
														<a href="#delete"
												class="btn btn--light btn-xs"
												onclick="deleteConfirm('<?php echo site_url('users/delete/' . $user->id); ?>');">Delete</a>
														<?php
													}
													else if($user->isSuper=='1' && chkaccess($userrole,'admin','delete',true)!='0'){
														// echo "show delete";
														?>
														<a href="#delete"
												class="btn btn--light btn-xs"
												onclick="deleteConfirm('<?php echo site_url('users/delete/' . $user->id); ?>');">Delete</a>
														<?php
													}
												}
												else if($this->session->userdata('logged_in')['isSuper']=='1'){
													?>
													<a href="#delete"
												class="btn btn--light btn-xs"
												onclick="deleteConfirm('<?php echo site_url('users/delete/' . $user->id); ?>');">Delete</a>
													<?php
												}
											}
											?>
										</td>
								</tr>
							<?php endforeach; ?>
						<?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
