<section id="content">

    <div class="card">

        <div class="card__header">

            <h2>Add New Staff</h2>

        </div>





        <div class="card__body">

            <div class="row">

				<?php if ($this->session->flashdata('success')) { ?>

					<div class="alert alert-success">

						<a href="#" class="close" data-dismiss="alert">&times;</a>

						<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>

					</div>

				<?php } else if ($this->session->flashdata('error')) { ?>

					<div class="alert alert-danger">

						<a href="#" class="close" data-dismiss="alert">&times;</a>

						<strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>

					</div>

				<?php } else if ($this->session->flashdata('warning')) { ?>

					<div class="alert alert-warning">

						<a href="#" class="close" data-dismiss="alert">&times;</a>

						<strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>

					</div>

				<?php } else if ($this->session->flashdata('info')) { ?>

					<div class="alert alert-info">

						<a href="#" class="close" data-dismiss="alert">&times;</a>

						<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>

					</div>

				<?php } ?>





				<?php if (validation_errors()): ?>

					<div class="col-md-12">

						<div id="validation_errors" title="Error:">

							<?php echo validation_errors(); ?>

						</div>

					</div>

				<?php endif; ?>





				<form id="group_add" action="<?php echo site_url('users/add'); ?>" method="post" >

                    <div class="input-group col-sm-12">

                        <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>



                        <div class="form-group">

                            <input type="text" name="name" id="name" class="form-control" placeholder="Name" />

							<i class="form-group__bar"></i>

                        </div>

                    </div>



                    <div class="input-group col-sm-12">

                        <span class="input-group-addon"><i class="zmdi zmdi-email"></i></span>



                        <div class="form-group">

                            <input type="email" name="email" id="email" class="form-control" placeholder="Email Address" />

                            <i class="form-group__bar"></i>

                        </div>

                    </div>



                    <div class="input-group col-sm-12">

                        <span class="input-group-addon"><i class="zmdi zmdi-shield-security"></i></span>



                        <div class="form-group">

                            <input type="password" name="password" id="password" class="form-control" placeholder="Password" />

							<i class="form-group__bar"></i>

                        </div>

                    </div>



                    <div class="input-group col-sm-12">

                        <span class="input-group-addon"><i class="zmdi zmdi-shield-security"></i></span>



                        <div class="form-group">

                            <input type="password" name="confirm" id="confirm" class="form-control" placeholder="Confirm Password" />

							<i class="form-group__bar"></i>

                        </div>

                    </div>



                    <div class="input-group col-sm-12">

                        <span class="input-group-addon"><i class="zmdi zmdi-local-phone"></i></span>



                        <div class="form-group">

                            <input type="text" name="phone" id="phone" class="form-control" placeholder="Phone" />

                            <i class="form-group__bar"></i>

                        </div>

                    </div>

                    <label class="select-role"><i class="zmdi zmdi-lock-open"></i>&nbsp;&nbsp;&nbsp; Select Staff Role</label>
                    <div class="input-group col-sm-12">

                        <!-- <span class="input-group-addon"><i class="zmdi zmdi-lock-open"></i></span> -->

                        <div class="form-group1">
                            <?php
                                foreach($permissions['records'] as $key=>$val){
                                    if($val['role_id']=='2' && $this->session->userdata('logged_in')['isSuper']!='2'){
                                        continue;
                                    }
                                    else if($val['role_id']=='1' && chkaccess($userrole,'admin','create',true)=='0'){
                                        continue;
                                    }
                                    else if($val['role_id']=='0' && chkaccess($userrole,'operator','create',true)=='0'){
                                        continue;
                                    }
                                    $role = '';
                                    if($val['role_id']=='0'){
                                        $role = 'Operator';
                                    }
                                    else if($val['role_id']=='1'){
                                        $role = 'Super User';
                                    }
                                    else if($val['role_id']=='2'){
                                        $role = 'Admin';
                                    }
                                    ?>
                                    <label class="radio-inline role-radio">
                                   <input type="radio" name="isSuper" value="<?php echo $val['role_id']; ?>"/> <?php echo $role; ?>
                                   </label>
                                    <?php
                                }
                            ?>
                            <!-- <select name="isSuper" id="isSuper" class="form-control-- select-role" style="cursor: pointer;">
                                <option value="">Select Role</option>
                                <?php
                                    foreach($permissions['records'] as $key=>$val){
                                        if($val['role_id']=='2' && $this->session->userdata('logged_in')['isSuper']!='2'){
                                            continue;
                                        }
                                        else if($val['role_id']=='1' && chkaccess($userrole,'admin','create',true)=='0'){
                                            continue;
                                        }
                                        else if($val['role_id']=='0' && chkaccess($userrole,'operator','create',true)=='0'){
                                            continue;
                                        }
                                        ?>
                                        <option value="<?php echo $val['role_id']; ?>"><?php echo $val['name']; ?></option>
                                        <?php
                                    }
                                ?>
                            </select> -->

                        </div>

                    </div>



					<br/>

                    <button class="btn btn-default">Save</button>

				</form>

            </div>

        </div>

    </div>

</section>

