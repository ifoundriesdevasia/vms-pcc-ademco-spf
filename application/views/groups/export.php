<?php
$colCnt = 11; // 12
?>
<style>
	.adminform .form-group:not(.form-group--float) {
		margin-bottom: 12px;
	}

	.pagination {
		float: right;
		margin-top: 0;
	}
</style>


<section id="content">
	<div class="card">
		<div class="card__header">
			<h2>Groups Report<small></small></h2>
		</div>


		<div class="card__body">
			<?php if ($this->session->flashdata('success')): ?>
				<div class="alert alert-success">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
				</div>
			<?php endif; ?>

			<?php if ($this->session->flashdata('error')): ?>
				<div class="alert alert-danger">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
				</div>
			<?php endif; ?>

			<?php if ($this->session->flashdata('warning')): ?>
				<div class="alert alert-warning">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>
				</div>
			<?php endif; ?>

			<?php if ($this->session->flashdata('info')): ?>
				<div class="alert alert-info">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
				</div>
			<?php endif; ?>


			<form name="adminform" class="adminform" action="<?php echo site_url('groups/export'); ?>" method="post">
				<div class="row">
					<div class="col-sm-2">
						<p>Group :</p>
					</div>

					<div class="col-sm-4">
						<div class="form-group">
							<?php echo $formhtml['group']; ?>
							<i class="form-group__bar"></i>
						</div>
					</div>
				</div>

				<input type="submit" value="Search" class="btn btn-default" />

				<a href="<?php echo site_url('groups/export'); ?>" class="btn btn-default">Refresh</a>
			</form>
		</div>
	</div>




	<div class="card">
		<div class="card__body">
			<div class="table-responsive">
				<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>Group Name</th>
							<th>User Name</th>
							<th>NRIC</th>
							<th>Designation</th>
							<th>Department</th>
							<th>Division</th>
							<th>Team</th>
							<th>Company</th>
							<!--<th>Disabled</th>-->
							<th>Card Number</th>
							<th>Family Number</th>
							<th>Type</th>
						</tr>
					</thead>

					<tbody>
						<?php if (empty($data)): ?>
							<tr>
								<td colspan="<?php echo $colCnt; ?>"><h3 style="color:red;">Entry not found!</h3></td>
							</tr>

						<?php else: ?>
							<?php foreach ($data as $row): ?>
								<tr>
									<td><?php echo $row->group_name; ?></td>
									<td><?php echo $row->name; ?></td>
									<td><?php echo $row->cf_nric; ?></td>
									<td><?php echo $row->cf_designation; ?></td>
									<td><?php echo $row->cf_department; ?></td>
									<td><?php echo $row->cf_division; ?></td>
									<td><?php echo $row->cf_team; ?></td>
									<td><?php echo $row->cf_company; ?></td>

									<!--<td><?php // echo $row->user_disabled? 'true' : 'false'; ?></td>-->

									<td><?php echo $row->card_number; ?></td>
									<td><?php echo $row->family_number; ?></td>
									<td><?php echo $row->userType; ?></td>
								</tr>
							<?php endforeach; ?>
						<?php endif; ?>
					</tbody>


					<?php if (!empty($data)): ?>
					<tfoot>
						<tr>
							<td colspan="<?php echo $colCnt; ?>">
								<a href="<?php echo $exportLink; ?>"
									class="btn btn-default"
								>Export</a>

								<?php if (!empty($pagelinks)): ?>
									<?php echo $pagelinks; ?>
									<div class="clearfix"></div>
								<?php endif; ?>
							</td>
						</tr>
					</tfoot>
					<?php endif; ?>
				</table>
			</div>
		</div>
	</div>
</section>


<?php if (false && !empty($group)): ?>
<script type="text/javascript">
	!function($){
		$(document).ready(function(){
			var $wrapper = $('#content');

//			setTimeout(refreshGroupsTapCount, getRefreshTime());

			function refreshGroupsTapCount() {
				var $ids = $wrapper.find('[data-type="group-tap-count"][data-tapid]'), ids = [];

				$ids.each(function(){
					ids.push($(this).data('tapid'));
				});

				$.getJSON(
					<?php echo json_encode($groupInfoUrl); ?>,
					{id: ids},
					function(resp){
						if('undefined' === typeof resp.items || !resp.items.length) {
							return;
						}

						for(var i in resp.items) {
							var item = resp.items[i];

							$wrapper.find('[data-tapid="'+item.id+'"]').each(function(){
								var $this = $(this);
								var num = ('escort' === $this.data('taptype'))?
									item.escort_in_count : item.visitor_in_count;

								$this.hide().html(num).fadeIn();
							});
						}

						setTimeout(refreshGroupsTapCount, getRefreshTime());
					}
				);
			}

			function getRefreshTime() {
				var min = 5, max = 10;
				return (Math.random() * (max - min) + min) * 1000;
			}
		});
	}(jQuery);
</script>
<?php endif; ?>
