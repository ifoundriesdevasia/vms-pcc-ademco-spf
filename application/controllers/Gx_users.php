<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gx_users extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->output->set_template('default');
		$this->load->model('gx_user_model', '', true);

		if (!$this->session->userdata('logged_in')) {
			// Allow some methods?
			$allowed = array(
				'login', 'updaterecord', 'updaterecordset', 'userupdate',
				'latestevent', 'userlist', 'cronscheduler', 'adduser',
				'showuserlist', 'fulluserdetails', 'updateuserstatus',
				'resetallvisitor', 'resetallescort',
			);

			if (!in_array($this->router->fetch_method(), $allowed)) {
				redirect('groups/index');
			}
		}
	}

	public function index() {
		redirect('groups/index');
	}

	public function refresh() {
		$this->output->set_template('default');

		$format = $this->input->post_get('format');
		$debug = $this->input->post_get('debug'); // debug
		$resp = $this->gx_user_model->refresh_users_status();

		$isNew = (empty($resp) || is_null($resp));
		$doRefresh = $isNew || empty($resp->process);

		$read = 0;
		$store = 0;
		$status = '-';
		$lastUpdate = '- never -';
		$errMsg = '';
		$debugMsg = '';
		$msg = array();

		if(isset($resp->stats->read)) {
			$read = $resp->stats->read;
			$store = $resp->stats->inserted;
		}

		if(!empty($resp->ended)) {
			$lastUpdate = date('Y-m-d H:i:s', $resp->ended);
		}

		if(!empty($resp->process)) {
			$status = 'In Progress';
		}
		else {
			if(!empty($resp->ended)) {
				$msg[0] = '<p>Data synced!</p>';
				$status = 'Completed';

				// debug
				if($debug && !empty($resp->stats->debugs)) {
					$debugMsg = 'Debug: <br/>';
					if(count($resp->stats->debugs) > 1) {
						$debugMsg .= '<ul>';

						foreach($resp->stats->debugs as $dmsg) {
							$debugMsg .= '<li><pre>' . print_r($dmsg, 1) . '</pre></li>';
						}

						$debugMsg .= '</ul><br/>';
					}
					else {
						$debugMsg .= '<pre>' . print_r($resp->stats->debugs[0], 1) . '</pre><br/>';
					}
				}

				// error?
				if(!empty($resp->stats->errors)) {
					$errMsg = 'Error(s): <br/>';
					if(count($resp->stats->errors) > 1) {
						$errMsg .= '<ul><li>' . implode('</li><li>', $resp->stats->errors) . '</li></ul><br/>';
					}
					else {
						$errMsg .= $resp->stats->errors[0] . '<br/>';
					}
				}
			}
		}

		$resp->statusUrl = $debug?
			site_url('gx-users/refresh/?format=json&debug=1'): site_url('gx-users/refresh/?format=json');
		$resp->refresh = $doRefresh;

		// refresh is not available when format in JSON (get status update only)
		if($doRefresh && 'json' !== $format) {
			$read = 0;
			$store = 0;

			$status = 'In Progress';
			$resp->refreshUrl = $debug?
				site_url('gx-users/do-refresh/?debug=1'): site_url('gx-users/do-refresh/');

			$msg[0] = <<<EOD
			<p>
				Syncing data...
				<span class="preloader preloader--sm preloader--light">
					<svg viewBox="25 25 50 50">
						<circle cx="50" cy="50" r="20"></circle>
					</svg>
				</span>
			</p>
EOD;
		}

		$msg[1] = <<<EOD
			<p>
				Record Read: $read<br/>
				Record Stored: $store<br/>
				Status: $status<br/>
				<!-- <em>Last Update: $lastUpdate</em>-->
				$errMsg
				$debugMsg
			</p>
EOD;

		$resp->msg = implode('', $msg);

		if('json' === $format) {
			echo json_encode($resp);
			exit;
		}

		$this->load->view('gx_users/refresh', array('response' => $resp));
	}

	public function doRefresh() {
		// close and unlock session file
		session_write_close();

		$this->output->set_template('blank');

		// debug?
		$debug = $this->input->post_get('debug'); // debug

		$data = $this->gx_user_model->refresh_users($debug);

		echo 'completed';
//		echo $debug? 'do debug' : 'x debug';
		exit;
	}

}
