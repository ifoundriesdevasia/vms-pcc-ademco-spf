-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 30, 2018 at 04:04 PM
-- Server version: 5.5.60-cll
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projects_spf`
--

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `group_name` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `group_name`, `created_by`, `created_time`) VALUES
(20, 'HOST1', 1, '2017-07-23 22:00:55'),
(22, 'Host 2', 1, '2017-07-28 02:58:47');

-- --------------------------------------------------------

--
-- Table structure for table `group_users`
--

DROP TABLE IF EXISTS `group_users`;
CREATE TABLE `group_users` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `CardNumber` int(11) DEFAULT NULL,
  `FamilyNumber` int(11) DEFAULT NULL,
  `has_tap` tinyint(1) NOT NULL,
  `identifier` int(11) NOT NULL,
  `tapin` varchar(255) NOT NULL,
  `tapout` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `group_users`
--

INSERT INTO `group_users` (`id`, `group_id`, `user_id`, `CardNumber`, `FamilyNumber`, `has_tap`, `identifier`, `tapin`, `tapout`) VALUES
(116, 20, 0, 45280, 25, 0, 0, '', ''),
(117, 20, 1, 445, 255, 0, 1, '', ''),
(118, 20, 2, 27547, 11, 0, 1, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `latest_event`
--

DROP TABLE IF EXISTS `latest_event`;
CREATE TABLE `latest_event` (
  `id` bigint(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `DoorName` varchar(100) NOT NULL,
  `DoorName2` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `latest_event`
--

INSERT INTO `latest_event` (`id`, `user_id`, `DoorName`, `DoorName2`) VALUES
(1, 0, 'OUTGOING READER', 'CTRL-DIN-01 RD1 DR 2');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `isSuper` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `phone`, `isSuper`, `created`, `modified`) VALUES
(1, 'Ujjwal Bera', 'ujjwalbera@mail.com', 'e11eef89bb7313aa6a7f9990a3335607', '8981987974', 1, '2017-08-10 02:50:11', '2017-08-10 02:53:59'),
(16, 'tetetetet', 'ajs6683@yahoo.com', 'cc03e747a6afbbcbf8be7668acfebee5', '1111111000', 1, '2017-08-10 02:51:02', '2017-08-16 05:17:26'),
(17, 'Ujjwal Bera', 'ujjwal@mailinator.com', '5d9a1838e802c365706b6a183b62a74b', '9832749810', 1, '2017-08-10 02:30:05', '2017-08-10 02:32:04'),
(20, 'Anna Aye', 'anna.aye@ademcosecurity.com', '16d7a4fca7442dda3ad93c9a726597e4', '', 1, '2017-08-23 05:43:29', '2017-08-23 05:43:29');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_users`
--
ALTER TABLE `group_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `latest_event`
--
ALTER TABLE `latest_event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `group_users`
--
ALTER TABLE `group_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
