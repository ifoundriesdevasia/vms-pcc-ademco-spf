ALTER TABLE `groups` DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

ALTER TABLE `group_users` DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

ALTER TABLE `latest_event` DEFAULT CHARSET=utf8 COLLATE utf8_general_ci; 

ALTER TABLE `users` DEFAULT CHARSET=utf8 COLLATE utf8_general_ci; 
